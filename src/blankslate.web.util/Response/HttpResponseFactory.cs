﻿using System.Net.Http;
using blankslate.util.Validation;
using blankslate.util.Windsor;

namespace blankslate.web.util.Response
{
    public class HttpResponseFactory
    {
        public static HttpResponseMessage CreateUsing<T>(IValidationResult result, HttpRequestMessage request)
        {
            var response = IoC.Resolve.Component<IFilteredResponse>();
            return response.Handle<T>(result, request);
        }
    }
}