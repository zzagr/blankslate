﻿using System.Net;
using System.Net.Http;
using blankslate.util.Logging;
using blankslate.util.Validation;

namespace blankslate.web.util.Response
{
    public class ForbiddenResponse : Loggable, IFilteredResponse
    {
        private readonly IFilteredResponse _successor;

        public ForbiddenResponse(IFilteredResponse successor)
        {
            _successor = successor;
        }

        public HttpResponseMessage Handle<T>(IValidationResult result, HttpRequestMessage request)
        {
            return !result.IsValid && result.Contains<Forbidden>() ? CreateErrorResponse(result, request) : _successor.Handle<T>(result, request);
        }

        private HttpResponseMessage CreateErrorResponse(IValidationResult result, HttpRequestMessage request)
        {
            Logger.Warning(result.Message);
            return request.CreateErrorResponse(HttpStatusCode.Forbidden, result.Message);
        }
    }
}