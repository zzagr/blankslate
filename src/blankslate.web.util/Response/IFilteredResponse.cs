﻿using System.Net.Http;
using blankslate.util.Validation;

namespace blankslate.web.util.Response
{
    public interface IFilteredResponse
    {
        HttpResponseMessage Handle<T>(IValidationResult result, HttpRequestMessage request);
    }
}