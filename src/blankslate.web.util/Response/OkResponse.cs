﻿using System.Net;
using System.Net.Http;
using blankslate.util.Validation;

namespace blankslate.web.util.Response
{
    public class OkResponse : IFilteredResponse
    {
        private readonly IFilteredResponse _successor;

        public OkResponse(IFilteredResponse successor)
        {
            _successor = successor;
        }

        public HttpResponseMessage Handle<T>(IValidationResult result, HttpRequestMessage request)
        {
            return result.IsValid ? request.CreateResponse(HttpStatusCode.OK, result.GetEntity<T>()) : _successor.Handle<T>(result, request);
        }
    }
}