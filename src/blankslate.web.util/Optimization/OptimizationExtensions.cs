﻿using System;
using System.Web.Mvc;
using System.Web.Optimization;
using Microsoft.Ajax.Utilities;

namespace blankslate.web.util.Optimization
{
    public static class OptimizationExtensions
    {
        //<summary>
        /*
            <script type="text/javascript">
                @(Html.JsMinify(@<text>
                // JavaScript Code Goes Here
                </text>))
            </script>
        */
        //</summary>
        public static MvcHtmlString JsMinify(this HtmlHelper helper, Func<object, object> markup)
        {
            if (helper == null || markup == null)
            {
                return MvcHtmlString.Empty;
            }

            var sourceJs = (markup.DynamicInvoke(helper.ViewContext) ?? string.Empty).ToString();

            if (!BundleTable.EnableOptimizations)
            {
                return new MvcHtmlString(sourceJs);
            }

            var minifier = new Minifier();

            var minifiedJs = minifier.MinifyJavaScript(sourceJs, new CodeSettings
            {
                EvalTreatment = EvalTreatment.MakeImmediateSafe,
                PreserveImportantComments = false
            });

            return new MvcHtmlString(minifiedJs);
        }

        //<summary>
        /*
            <style type="text/css">
                @(Html.CssMinify(@<text>
                // CSS Goes Here
                </text>))
            </style>
        */
        //</summary>
        public static MvcHtmlString CssMinify(this HtmlHelper helper, Func<object, object> markup)
        {
            if (helper == null || markup == null)
            {
                return MvcHtmlString.Empty;
            }

            var sourceCss = (markup.DynamicInvoke(helper.ViewContext) ?? string.Empty).ToString();

            if (!BundleTable.EnableOptimizations)
            {
                return new MvcHtmlString(sourceCss);
            }

            var minifier = new Minifier();

            var minifiedCss = minifier.MinifyStyleSheet(sourceCss, new CssSettings
            {
                CommentMode = CssComment.None
            });

            return new MvcHtmlString(minifiedCss);
        }
    }
}