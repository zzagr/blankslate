﻿namespace blankslate.web.util.Optimization.Orderers
{
    public class AngularJsBundleOrderer : BaseScriptBundleOrderer
    {
        public AngularJsBundleOrderer()
            : base(new[]
            {
                new ScriptGroup {Priority = 1, Key = "module"},
                new ScriptGroup {Priority = 2, Key = "constants"},
                new ScriptGroup {Priority = 3, Key = "config"},
                new ScriptGroup {Priority = 4, Key = "runner"},
                new ScriptGroup {Priority = 5, Key = "route"},
                new ScriptGroup {Priority = 6, Key = "task"},
                new ScriptGroup {Priority = 7, Key = "service"},
                new ScriptGroup {Priority = 8, Key = "directive"},
                new ScriptGroup {Priority = 9, Key = "controller"}
            })
        {
        }
    }
}