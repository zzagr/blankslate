﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Optimization;
using blankslate.util;

namespace blankslate.web.util.Optimization.Orderers
{
    public abstract class BaseScriptBundleOrderer : IBundleOrderer
    {
        private readonly Dictionary<ScriptGroup, List<BundleFile>> _itemGroups;
        private readonly ScriptGroup[] _scriptGroups;

        protected BaseScriptBundleOrderer(ScriptGroup[] scriptGroups)
        {
            _itemGroups = new Dictionary<ScriptGroup, List<BundleFile>>();
            _scriptGroups = scriptGroups;
        }

        public IEnumerable<BundleFile> OrderFiles(BundleContext context, IEnumerable<BundleFile> files)
        {
            InitializeAngularGroups();

            AddFiles(files);

            var scriptFiles = _itemGroups.SelectMany(s => s.Value);


            return scriptFiles;
        }

        private void AddFiles(IEnumerable<BundleFile> files)
        {
            foreach (var itemGroup in _itemGroups)
            {
                var key = itemGroup.Key;
                var groupedFiles = files.Where(f => f.VirtualFile.Name != null && f.VirtualFile.Name.ToLower().Contains("{0}.js".With(key.Key)));
                _itemGroups[key].AddRange(groupedFiles);
            }
        }

        private void InitializeAngularGroups()
        {
            Array.Sort(_scriptGroups);

            foreach (var angularGroup in _scriptGroups)
            {
                if (_itemGroups.ContainsKey(angularGroup)) continue;
                _itemGroups.Add(angularGroup, new List<BundleFile>());
            }
        }
    }
}