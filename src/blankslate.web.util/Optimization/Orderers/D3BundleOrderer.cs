﻿namespace blankslate.web.util.Optimization.Orderers
{
    public class D3BundleOrderer : BaseScriptBundleOrderer
    {
        public D3BundleOrderer() : base(new[]
        {
            new ScriptGroup {Priority = 1, Key = "grouping"},
            new ScriptGroup {Priority = 2, Key = "chart"},
            new ScriptGroup {Priority = 3, Key = "data.provider"},
            new ScriptGroup {Priority = 4, Key = "pipeline"}
        })
        {
        }
    }
}