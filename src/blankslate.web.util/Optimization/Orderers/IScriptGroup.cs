﻿using System;

namespace blankslate.web.util.Optimization.Orderers
{
    public interface IScriptGroup<T> : IComparable<T> where T : class
    {
        int Priority { get; set; }
        string Key { get; set; } 
    }
}