﻿namespace blankslate.web.util.Optimization.Orderers
{
    public class ScriptGroup : IScriptGroup<ScriptGroup>
    {
        public int CompareTo(ScriptGroup other)
        {
            if (Priority > other.Priority)
                return 1;
            if (Priority < other.Priority)
                return -1;
            return 0;
        }

        public override bool Equals(object obj)
        {
            if (this == obj) return true;
            var other = obj as ScriptGroup;
            if (other == null) return false;
            return Key == other.Key;
        }

        public override int GetHashCode()
        {
            return Key.GetHashCode();
        }

        public int Priority { get; set; }
        public string Key { get; set; }
    }
}