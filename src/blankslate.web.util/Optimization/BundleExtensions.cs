﻿using System.Web.Optimization;
using BundleTransformer.Core.Orderers;

namespace blankslate.web.util.Optimization
{
    public static class BundleExtentions
    {
        private static readonly NullOrderer NullOrderer;

        static BundleExtentions()
        {
            NullOrderer = new NullOrderer();
        }


        public static Bundle AddBundle(this BundleCollection bundles, string bundleName, string direcotry, string wildCard, IBundleOrderer orderer = null)
        {
            var appScripts = new ScriptBundle(bundleName)
                .IncludeDirectory(direcotry, wildCard, true);

            appScripts.Orderer = orderer ?? NullOrderer;
            bundles.Add(appScripts);

            return appScripts;
        }
    }
}