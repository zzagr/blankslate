﻿using System.Web.Mvc;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using blankslate.util.Windsor;

namespace blankslate.web.util.Windsor
{
    public class MvcControllerInstaller<TController> : IWindsorInstaller where TController : IController
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Classes.FromAssemblyContaining(typeof (TController)).BasedOn<IController>().LifestyleTransient().WithLoggingInterceptor());
        }
    }
}