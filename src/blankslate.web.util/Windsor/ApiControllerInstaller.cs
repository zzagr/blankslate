﻿using System.Web.Http.Controllers;
using blankslate.util.Configuration;
using blankslate.util.Windsor;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using blankslate.web.util.Controller;
using blankslate.web.util.Response;

namespace blankslate.web.util.Windsor
{
    public class ApiControllerInstaller<TController> : IWindsorInstaller where TController : IHttpController
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Classes.FromAssemblyContaining(typeof (TController)).BasedOn<BaseApiController>().LifestyleTransient().WithLoggingInterceptor());

            RegisterHttpStatusCodes(container);
        }

        private static void RegisterHttpStatusCodes(IWindsorContainer container)
        {
            container.Register(Classes.FromAssemblyContaining<IAppSettings>().BasedOn<IAppSettings>().WithServiceFirstInterface().LifestyleTransient());

            //HttpStatus.BadRequest
            container.Register(Component.For<IFilteredResponse>().ImplementedBy<RequiredResponse>());

            //HttpStatus.Forbidden
            container.Register(Component.For<IFilteredResponse>().ImplementedBy<ForbiddenResponse>()
                .DependsOn(Dependency.OnComponent<IFilteredResponse, RequiredResponse>()));

            //HttpStatus.InternalServerError
            container.Register(Component.For<IFilteredResponse>().ImplementedBy<ServerErrorResponse>()
                .DependsOn(Dependency.OnComponent<IFilteredResponse, ForbiddenResponse>()));

            //HttpStatus.NotFound
            container.Register(Component.For<IFilteredResponse>().ImplementedBy<NotFoundResponse>()
                .DependsOn(Dependency.OnComponent<IFilteredResponse, ServerErrorResponse>()));

            //HttpStatus.BadRequest
            container.Register(Component.For<IFilteredResponse>().ImplementedBy<GenericErrorResponse>()
                .DependsOn(Dependency.OnComponent<IFilteredResponse, NotFoundResponse>()));

            //HttpStatus.OK
            container.Register(Component.For<IFilteredResponse>().ImplementedBy<OkResponse>().IsDefault()
                .DependsOn(Dependency.OnComponent<IFilteredResponse, GenericErrorResponse>()));
        }
    }
}