﻿using System.Web.Http.ExceptionHandling;
using blankslate.util.Configuration;
using blankslate.util.Logging;

namespace blankslate.web.util.WebApi
{
    public class UnhandledExceptionLogger : ExceptionLogger
    {
        private static readonly ILogger Logger;

        static UnhandledExceptionLogger()
        {
            Logger = new ConsoleLogger();
        }

        public override void Log(ExceptionLoggerContext context)
        {
            Logger.Fatal(context.ExceptionContext.Exception);
        }
    }
}