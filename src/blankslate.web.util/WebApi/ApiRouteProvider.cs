﻿using System;
using System.Web.Http.Controllers;
using System.Web.Http.Routing;
using blankslate.web.util.Controller;

namespace blankslate.web.util.WebApi
{
    public class ApiRouteProvider : DefaultDirectRouteProvider
    {
        protected override string GetRoutePrefix(HttpControllerDescriptor controllerDescriptor)
        {
            var routePrefix = base.GetRoutePrefix(controllerDescriptor);
            var controllerBaseType = controllerDescriptor.ControllerType.BaseType;

            if (controllerBaseType == typeof (BaseApiController))
            {
                routePrefix = BasePrefix + routePrefix;
            }

            return routePrefix;
        }

        protected virtual string BasePrefix 
        {
            get { return "api/";  }
        }

        protected virtual Type ControlelrType
        {
            get { return typeof (BaseApiController);  }
        }


    }


}