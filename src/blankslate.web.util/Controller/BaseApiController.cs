﻿using System;
using System.Net;
using System.Net.Http;
using System.Security;
using System.Web.Http;
using blankslate.util;
using blankslate.util.Logging;
using blankslate.util.Validation;
using blankslate.util.Windsor;
using blankslate.web.util.Response;

namespace blankslate.web.util.Controller
{
    public class BaseApiController : ApiController
    {
        public ILogger Logger
        {
            get { return IoC.Resolve.Component<ILogger>(); }
        }

        protected HttpResponseMessage CreateHttpResponse<TResponse>(HttpRequestMessage request,
            Func<IValidationResult> codeToExecute)
        {
            try
            {
                var validationResult = codeToExecute.Invoke();
                return HttpResponseFactory.CreateUsing<TResponse>(validationResult, request);
            }
            catch (ArgumentException ex)
            {
                Logger.Error(ex);
                return request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.ShowAllMessages());
            }
            catch (SecurityException ex)
            {
                Logger.Error(ex);
                return request.CreateErrorResponse(HttpStatusCode.Unauthorized, ex.ShowAllMessages());
            }
            catch (Exception ex)
            {
                Logger.Fatal(ex);
                return request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.ShowAllMessages());
            }
        }
    }
}