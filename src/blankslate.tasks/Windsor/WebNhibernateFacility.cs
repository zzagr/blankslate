﻿using System;
using System.IO;
using System.Reflection;
using Castle.MicroKernel.Facilities;
using Castle.MicroKernel.Registration;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using blankslate.util.Configuration;
using NHibernate;
using NHibernate.Event;

namespace blankslate.tasks.Windsor
{
    public class WebNhibernateFacility : AbstractFacility
    {
        protected override void Init()
        {
            var settings = new WebSettings();

            var noDb = settings.GetApplicationDetails().Configuration.NoDb;
            if (noDb) return;

            Kernel.Register(
                Component.For<ISessionFactory>()
                    .UsingFactoryMethod(k => CreateSessionFactory(k.Resolve<IWebSettings>(), "DefaultConnection", "blankslate.dataaccess.SqlServer.dll"))
                    .LifestyleTransient(),
                Component.For<ISession>()
                    .UsingFactoryMethod(k => k.Resolve<ISessionFactory>().OpenSession())
                    .LifestyleTransient()
                );
        }

        private static ISessionFactory CreateSessionFactory(IAppSettings settings, string connectionStringKey, string assemblyName)
        {
            var binDirectory = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "bin");
            var dataAccessLocation = Path.Combine(binDirectory, assemblyName);

            var databaseConfiguration = MsSqlConfiguration.MsSql2008.ConnectionString(settings.GetConnectionString(connectionStringKey));
            return Fluently.Configure()
                .Database(databaseConfiguration)
                .Mappings(m => m.FluentMappings.AddFromAssembly(Assembly.LoadFrom(dataAccessLocation)))
                .ExposeConfiguration(c =>
                {
                    c.SetListener(ListenerType.PreInsert, new AuditEventListener());
                    c.SetListener(ListenerType.PreUpdate, new AuditEventListener());
                })
                .BuildSessionFactory();
        }
    }
}