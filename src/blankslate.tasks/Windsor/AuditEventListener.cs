﻿using System;
using blankslate.util;
using blankslate.util.Domain;
using NHibernate.Event;
using NHibernate.Persister.Entity;

namespace blankslate.tasks.Windsor
{
    public class AuditEventListener : IPreUpdateEventListener, IPreInsertEventListener
    {
        public bool OnPreInsert(PreInsertEvent @event)
        {
            var entity = @event.Entity as IEntity;
            if (entity == null) return false;

            var createdTimestamp = DateTime.UtcNow;
            var modifiedTimestamp = DateTime.UtcNow;

            SyncState(@event.Persister, @event.State, PropertyName.For<IEntity>(e => e.CreatedTimestamp), createdTimestamp);
            SyncState(@event.Persister, @event.State, PropertyName.For<IEntity>(e => e.LastModifiedTimestamp), modifiedTimestamp);
            entity.CreatedTimestamp = createdTimestamp;
            entity.LastModifiedTimestamp = modifiedTimestamp;

            return false;
        }

        public bool OnPreUpdate(PreUpdateEvent @event)
        {
            var entity = @event.Entity as IEntity;
            if (entity == null) return false;

            var modifiedTimestamp = DateTime.UtcNow;

            SyncState(@event.Persister, @event.State, PropertyName.For<IEntity>(e => e.LastModifiedTimestamp), modifiedTimestamp);
            entity.LastModifiedTimestamp = modifiedTimestamp;


            return false;
        }

        public void SyncState(IEntityPersister persister, object[] state, string propertyName, object value)
        {
            var index = Array.IndexOf(persister.PropertyNames, propertyName);
            if (index == -1) return;
            state[index] = value;
        }
    }
}