﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.Resolvers.SpecializedResolvers;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using blankslate.dataaccess;
using blankslate.dataaccess.sql;
using blankslate.dataaccess.sql;
using blankslate.tasks.Services;
using blankslate.util.Mapping;
using blankslate.util.Validation;
using blankslate.util.Windsor;

namespace blankslate.tasks.Windsor
{
    public class TaskContainerInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Kernel.Resolver.AddSubResolver(new CollectionResolver(container.Kernel));
            container.Install(new TaskDecoratorInstaller(), new UtilInstaller());
            container.Register(Classes.FromThisAssembly().BasedOn<ITask>().WithServiceDefaultInterfaces().LifestyleTransient().WithLoggingInterceptor());
            container.Register(Classes.FromThisAssembly().BasedOn<IService>().WithServiceDefaultInterfaces().LifestyleTransient().WithLoggingInterceptor());
            container.Register(Classes.FromThisAssembly().BasedOn<IValidator>().WithServiceFirstInterface());
            container.Register(Classes.FromAssemblyContaining<BaseFetchStrategy>().BasedOn<IFetchStrategy>().WithServiceFirstInterface().LifestyleTransient());
            container.Register(Classes.FromThisAssembly().BasedOn(typeof (IMapper<,>)).WithService.Base());
        }
    }
}