﻿using blankslate.util.Validation;

namespace blankslate.tasks
{
    public interface IPingTask : ITask
    {
        IValidationResult Ping();
    }
}