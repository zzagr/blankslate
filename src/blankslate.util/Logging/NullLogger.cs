﻿using System;

namespace blankslate.util.Logging
{
    public class NullLogger : ILogger
    {
        private const string LoggerError = "Logger not configured. Did you install ILogger on IoC?";

        public void Info(string format, params object[] args)
        {
            throw new ApplicationException(LoggerError);
        }

        public void Debug(string format, params object[] args)
        {
            throw new ApplicationException(LoggerError);
        }

        public void Warning(string message)
        {
            throw new ApplicationException(LoggerError);
        }

        public void Error(string message)
        {
            throw new ApplicationException(LoggerError);
        }

        public void Error(string message, Exception exception)
        {
            throw new ApplicationException(LoggerError);
        }

        public void Error(Exception ex)
        {
            throw new ApplicationException(LoggerError);
        }

        public void Fatal(string message)
        {
            throw new ApplicationException(LoggerError);
        }

        public void Fatal(string message, Exception exception)
        {
            throw new ApplicationException(LoggerError);
        }

        public void Fatal(Exception ex)
        {
            throw new ApplicationException(LoggerError);
        }
    }
}