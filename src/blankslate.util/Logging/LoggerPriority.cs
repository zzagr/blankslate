﻿namespace blankslate.util.Logging
{
    public enum LoggerPriority
    {
        
        Default = 0,
        Debug = 100,
        Info = 200,
        Warning = 300,
        Error = 400,
        Fatal = 500
    };
}