﻿using System;

namespace blankslate.util.Logging
{
    public class ConsoleLogger : ILogger
    {
        public void Info(string format, params object[] args)
        {
            WriteEntry("Info", format, args);
        }

        public void Debug(string format, params object[] args)
        {
            WriteEntry("Debug", format, args);
        }

        public void Warning(string message)
        {
            WriteEntry("Warning", message);
        }

        public void Error(string message)
        {
            WriteEntry("Error", message);
        }

        public void Error(string message, Exception exception)
        {
            WriteEntry("Error", message, exception.ShowAllMessages());
        }

        public void Error(Exception ex)
        {
            WriteEntry("Error", ex.ShowAllMessages());
        }

        public void Fatal(string message)
        {
            WriteEntry("Fatal", message);
        }

        public void Fatal(string message, Exception exception)
        {
            WriteEntry("Fatal", message, exception.ShowAllMessages());
        }

        public void Fatal(Exception ex)
        {
            WriteEntry("Fatal", ex.ShowAllMessages());
        }

        protected void WriteEntry(string severity, string message, params object[] args)
        {
            Console.Out.WriteLine("{0}[{1}] - {2}", DateTime.Now.ToString("g"), severity, message.With(args));
        }
    }
}