﻿namespace blankslate.util.Logging
{
    public abstract class Loggable
    {
        private ILogger _logger = new NullLogger();

        public ILogger Logger
        {
            get { return _logger; }
            set { _logger = value; }
        }
    }
}