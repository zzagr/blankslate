﻿using System;
using System.Diagnostics;
using blankslate.util.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using EntlibLogger = Microsoft.Practices.EnterpriseLibrary.Logging.Logger;


namespace blankslate.util.Logging
{
    public class EntlibLogger : ILogger
    {
        private readonly string _title;

        static EntlibLogger()
        {
            Logger.SetLogWriter(new LogWriterFactory().Create());
        }

        public EntlibLogger(IAppSettings settings)
        {
            var details = settings.GetApplicationDetails();
            _title = "{0}.{1}".With(details.Environment, details.Name);
        }

        public void Info(string format, params object[] args)
        {
            var entry = new LogEntry
            {
                Message = format.With(args),
                Priority = LoggerPriority.Info.ToInteger(),
                Severity = TraceEventType.Information
            };

            WriteEntry(entry);
        }

        public void Debug(string format, params object[] args)
        {
            var entry = new LogEntry
            {
                Message = format.With(args),
                Priority = LoggerPriority.Debug.ToInteger(),
                Severity = TraceEventType.Verbose
            };

            WriteEntry(entry);
        }

        public void Warning(string message)
        {
            var entry = new LogEntry
            {
                Message = message,
                Priority = LoggerPriority.Warning.ToInteger(),
                Severity = TraceEventType.Warning
            };

            WriteEntry(entry);
        }

        public void Error(string message)
        {
            var entry = new LogEntry
            {
                Message = message,
                Priority = LoggerPriority.Error.ToInteger(),
                Severity = TraceEventType.Error
            };

            WriteEntry(entry);
        }

        public void Error(string message, Exception exception)
        {
            var entry = new LogEntry
            {
                Message = "{0}\r\n{1}".With(message, exception.ShowAllMessages()),
                Priority = LoggerPriority.Error.ToInteger(),
                Severity = TraceEventType.Error
            };

            WriteEntry(entry);
        }

        public void Error(Exception ex)
        {
            var entry = new LogEntry
            {
                Message = ex.ShowAllMessages(),
                Priority = LoggerPriority.Error.ToInteger(),
                Severity = TraceEventType.Error
            };

            WriteEntry(entry);
        }

        public void Fatal(string message)
        {
            var entry = new LogEntry
            {
                Message = message,
                Priority = LoggerPriority.Fatal.ToInteger(),
                Severity = TraceEventType.Critical
            };

            WriteEntry(entry);
        }

        public void Fatal(string message, Exception exception)
        {
            var entry = new LogEntry
            {
                Message = "{0}\r\n{1}".With(message, exception.ShowAllMessages()),
                Priority = LoggerPriority.Fatal.ToInteger(),
                Severity = TraceEventType.Critical
            };

            WriteEntry(entry);
        }

        public void Fatal(Exception ex)
        {
            var entry = new LogEntry
            {
                Message = ex.ShowAllMessages(),
                Priority = LoggerPriority.Fatal.ToInteger(),
                Severity = TraceEventType.Critical
            };

            WriteEntry(entry);
        }

        protected virtual void WriteEntry(LogEntry entry)
        {
            var newMessage = "{0} {1}".With(GetCaller(), entry.Message);
            entry.Message = newMessage;
            entry.Title = _title;
            Logger.Writer.Write(entry);
        }

        private static string GetCaller()
        {
            var stackTrace = new StackTrace();
            var stackFrame = stackTrace.GetFrame(3);
            var methodBase = stackFrame.GetMethod();
            var className = methodBase.DeclaringType == null ? "Unknown Class" : methodBase.DeclaringType.Name;
            var methodName = methodBase.Name;

            return "Calling Method: {0}.{1}()".With(className, methodName);
        }
    }
}