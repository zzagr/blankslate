﻿using System;

namespace blankslate.util.Logging
{
    public interface ILogger
    {
        void Info(string format, params object[] args);
        void Debug(string format, params object[] args);
        void Warning(string message);
        void Error(string message);
        void Error(string message, Exception exception);
        void Error(Exception ex);
        void Fatal(string message);
        void Fatal(string message, Exception exception);
        void Fatal(Exception ex);
    }
}