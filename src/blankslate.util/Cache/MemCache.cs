﻿using System.Runtime.Caching;
using blankslate.util.Configuration;

namespace blankslate.util.Cache
{
    public class MemCache : IMemCache
    {
        private readonly MemoryCache _cache;

        public MemCache(IAppSettings settings)
        {
            var details = settings.GetApplicationDetails();
            var cacheName = "{0}.{1}".With(details.Environment, details.Name);
            _cache = new MemoryCache(cacheName);
        }

        public void Add(CacheEntry entry)
        {
            if (_cache.Contains(entry.Key))
                _cache.Remove(entry.Key);

            _cache.Add(entry.Key, entry.Item, entry.CacheExpiry);
        }

        public T Get<T>(string key)
        {
            if (_cache.Contains(key))
                return (T) _cache[key];

            return default(T);
        }
    }
}