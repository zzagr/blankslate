﻿namespace blankslate.util.Cache
{
    public interface IMemCache
    {
        void Add(CacheEntry entry);
        T Get<T>(string key);
    }
}