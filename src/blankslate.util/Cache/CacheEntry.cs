﻿using System;

namespace blankslate.util.Cache
{
    public class CacheEntry
    {
        public string Key { get; set; }
        public object Item { get; set; }
        public DateTime CacheExpiry { get; set; }

        private CacheEntry(string key, object item, DateTime cacheExpiry)
        {
            Key = key;
            Item = item;
            CacheExpiry = cacheExpiry;
        }

        public static CacheEntry TenSeconds(string key, object item)
        {
            return new CacheEntry(key, item, DateTime.Now.AddSeconds(10));
        }

        public static CacheEntry ThirtySeconds(string key, object item)
        {
            return new CacheEntry(key, item, DateTime.Now.AddSeconds(30));
        }

        public static CacheEntry FiveMinutes(string key, object item)
        {
            return new CacheEntry(key, item, DateTime.Now.AddMinutes(5));
        }
    }
}