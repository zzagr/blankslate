﻿using System;
using System.Linq.Expressions;

namespace blankslate.util
{
    public class PropertyName
    {
        public static string For<T>(Expression<Func<T, object>> expression)
        {
            var body = expression.Body;
            return GetMemberName(body);
        }

        public static string For(Expression<Func<object>> expression)
        {
            var body = expression.Body;
            return GetMemberName(body);
        }

        public static string GetMemberName(Expression expression)
        {
            var memberExpression = expression as MemberExpression;
            if (memberExpression != null)
                return memberExpression.Expression.NodeType == ExpressionType.MemberAccess ? "{0}.{1}".With(GetMemberName(memberExpression.Expression), memberExpression.Member.Name) : memberExpression.Member.Name;

            var unaryExpression = expression as UnaryExpression;
            if (unaryExpression == null)
                throw new Exception("Could not determine member from {0}".With(expression));
            if (unaryExpression.NodeType != ExpressionType.Convert)
                throw new Exception("Cannot interpret member from {0}".With(expression));

            return GetMemberName(unaryExpression.Operand);
        }
    }
}