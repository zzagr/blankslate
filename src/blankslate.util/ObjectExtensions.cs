﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using Newtonsoft.Json;

namespace blankslate.util
{
    public static class ObjectExtensions
    {
        public static string ToJson(this object item)
        {
            var settings = new JsonSerializerSettings
            {
                TypeNameHandling = TypeNameHandling.None
            };

            return JsonConvert.SerializeObject(item, settings);
        }

        public static IEnumerable<string> GetPublicProperties(this Type type)
        {
            var properties = new List<string>();
            var props = TypeDescriptor.GetProperties(type);
            for (var i = 0; i < props.Count; i++)
            {
                var prop = props[i];
                properties.Add(prop.Name);
            }

            return properties.OrderBy(property => property);
        }

        public static string GetPropertyName<T>(this T item, Expression<Func<T, object>> expression)
        {
            return PropertyName.For(expression);
        }

        public static int? ToNullableInteger(this object obj)
        {
            if (obj == null) return null;
            if (obj == DBNull.Value) return null;
            if (string.IsNullOrWhiteSpace(obj.ToString())) return null;

            return Convert.ToInt32(obj);
        }

        public static decimal? ToNullableDecimal(this object obj)
        {
            if (obj == null) return null;
            if (obj == DBNull.Value) return null;
            if (string.IsNullOrWhiteSpace(obj.ToString())) return null;

            return Convert.ToDecimal(obj);
        }

        public static int ToInteger(this object obj)
        {
            if (obj == null) return 0;
            if (obj == DBNull.Value) return 0;
            if (string.IsNullOrWhiteSpace(obj.ToString())) return 0;

            try
            {
                return Convert.ToInt32(obj);
            }
            catch (FormatException)
            {
                throw new ArgumentException("Cannot convert {0} to integer.".With(obj.ToString()));
            }
        }

        public static bool? ToNullableBoolean(this object obj)
        {
            if (obj == null) return null;
            if (obj == DBNull.Value) return null;
            if (string.IsNullOrWhiteSpace(obj.ToString())) return null;

            return Convert.ToBoolean(obj);
        }
    }
}