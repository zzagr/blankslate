﻿using System;

namespace blankslate.util.Domain
{
    public abstract class EntityDetail<TParent> : Entity<TParent>, IEntityDetail<TParent> where TParent : class, IEntity
    {
        public virtual TParent Parent { get; set; }
        public virtual DateTime StartEffectiveDate { get; set; }
        public virtual DateTime? EndEffectiveDate { get; set; }

        protected EntityDetail()
        {
            StartEffectiveDate = DateTime.UtcNow;
        }

        public virtual bool IsCurrentlyEffective()
        {
            return EndEffectiveDate == null;
        }

        public virtual void Expire()
        {
            EndEffectiveDate = DateTime.UtcNow;
        }
    }
}