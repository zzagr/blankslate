﻿using System.Collections.Generic;
using System.Linq;

namespace blankslate.util.Domain
{
    public abstract class EntityParent<TParent, TChild> : Entity<TParent>
        where TChild : EntityDetail<TParent>
        where TParent : class, IEntity
    {
        private ICollection<TChild> _details;

        protected EntityParent(ICollection<TChild> details)
        {
            _details = details;
        }

        public virtual ICollection<TChild> Details
        {
            get { return _details; }
            set { _details = value; }
        }

        public virtual TChild EffectiveDetail
        {
            get { return Details.First(detail => detail.IsCurrentlyEffective()); }
        }

        public virtual void CreateNewDetail(TChild detail)
        {
            if (Details.Count > 0)
                EffectiveDetail.Expire();

            AssignParent(detail);
            Details.Add(detail);
        }

        protected abstract void AssignParent(TChild detail);
    }
}