﻿using System;

namespace blankslate.util.Domain
{
    public interface IEntityDetail<TParent> : IEntity
    {
        TParent Parent { get; set; }
        DateTime StartEffectiveDate { get; set; }
        DateTime? EndEffectiveDate { get; set; }
    }
}