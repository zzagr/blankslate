﻿using System;

namespace blankslate.util.Domain
{
    public interface IEntity
    {
        int Id { get; set; }
        DateTime CreatedTimestamp { get; set; }
        DateTime LastModifiedTimestamp { get; set; }
        string LastModifiedBy { get; set; }
    }
}