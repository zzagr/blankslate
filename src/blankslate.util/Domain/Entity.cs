﻿using System;

namespace blankslate.util.Domain
{
    public abstract class Entity<T> : IEntity where T : class, IEntity
    {
        protected Entity()
        {
            CreatedTimestamp = DateTime.UtcNow;
            LastModifiedTimestamp = DateTime.UtcNow;
        }

        public virtual int Id { get; set; }
        public virtual DateTime CreatedTimestamp { get; set; }
        public virtual DateTime LastModifiedTimestamp { get; set; }
        public virtual string LastModifiedBy { get; set; }

        public override bool Equals(object obj)
        {
            if (this == obj) return true;
            var other = obj as T;
            if (other == null) return false;
            return Id == other.Id;
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }
    }
}