﻿namespace blankslate.util.Validation
{
    public interface IValidator<in T> : IValidator where T : class
    {
        ValidationResult Validate(T item);
    }

    public interface IValidator
    {
        //Marker Interface
    }
}