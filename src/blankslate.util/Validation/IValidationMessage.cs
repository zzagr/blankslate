﻿namespace blankslate.util.Validation
{
    public interface IValidationMessage
    {
        string Code { get; }
        string Message { get; }
    }
}