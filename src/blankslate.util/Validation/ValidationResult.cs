﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace blankslate.util.Validation
{
    public class ValidationResult : IValidationResult
    {
        private ICollection<IValidationMessage> _messages;
        public bool IsValid { get; set; }
        private object Entity { get; set; }

        public string Message
        {
            get
            {
                var messages = _messages.Select(GetMessage);
                return string.Join(", ", messages);
            }
        }

        public IValidationMessage[] Messages
        {
            get { return _messages.ToArray(); }
        }

        public int ErrorCount
        {
            get { return _messages.Count; }
        }

        public ValidationResult(bool isValid, string message)
        {
            IsValid = isValid;

            _messages = new List<IValidationMessage>();

            if (string.IsNullOrEmpty(message)) return;
            IsValid = false;
            _messages.Add(new GenericError(message));
        }

        public ValidationResult()
            : this(true, null)
        {
        }

        public ValidationResult Invalidate(string message)
        {
            return Invalidate(new GenericError(message));
        }

        public ValidationResult Invalidate(Exception ex)
        {
            return Invalidate(new ServerError(ex));
        }

        public ValidationResult Invalidate(IValidationMessage message)
        {
            IsValid = false;
            _messages.Add(message);

            return this;
        }

        public void Reset()
        {
            IsValid = true;
            _messages = new List<IValidationMessage>();
        }

        public ValidationResult SetEntity(object obj)
        {
            if (obj == null) throw new ArgumentNullException("Can't set null when setting an entity on ValidationResult!");
            Entity = obj;
            return this;
        }

        public T GetEntity<T>()
        {
            return (T)Entity;
        }

        public object GetEntity()
        {
            return Entity;
        }

        public Type GetEntityType()
        {
            return Entity.GetType();
        }

        private static string GetMessage(IValidationMessage message)
        {
            return message.GetType() == typeof(GenericError) ? message.Message : "[{0}] {1}".With(message.Code, message.Message);
        }

        public IValidationMessage[] GetErrorOfType<T>()
        {
            return _messages.Where(m => m.GetType() == typeof(T)).ToArray();
        }

        public bool Contains<T>()
        {
            foreach (var message in _messages)
            {
                if (message.GetType() == typeof(T)) return true;
            }
            return false;
        }

        public void Append(ValidationResult result)
        {
            foreach (var message in result.Messages)
            {
                _messages.Add(message);
            }
        }
    }
}