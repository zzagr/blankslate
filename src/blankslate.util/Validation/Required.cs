﻿namespace blankslate.util.Validation
{
    public class Required : IValidationMessage
    {
        public string Code { get; private set; }
        public string Message { get; private set; }

        public Required(string property)
        {
            Code = "Required";
            Message = "The property '{0}' is required.".With(property);
        }
    }
}