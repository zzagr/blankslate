using System;

namespace blankslate.util.Validation
{
    public interface IValidationResult
    {
        bool IsValid { get; set; }
        string Message { get; }
        IValidationMessage[] Messages { get; }
        int ErrorCount { get; }
        ValidationResult Invalidate(string message);
        ValidationResult Invalidate(Exception ex);
        ValidationResult Invalidate(IValidationMessage message);
        void Reset();
        ValidationResult SetEntity(object obj);
        T GetEntity<T>();
        object GetEntity();
        Type GetEntityType();
        IValidationMessage[] GetErrorOfType<T>();
        bool Contains<T>();
        void Append(ValidationResult result);
    }
}