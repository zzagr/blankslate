﻿namespace blankslate.util.Validation
{
    public class GenericError : IValidationMessage
    {
        public string Code { get; private set; }
        public string Message { get; private set; }

        public GenericError(string message)
        {
            Code = "GenericError";
            Message = message;
        }
    }
}