﻿namespace blankslate.util.Validation
{
    public class NotFound : IValidationMessage
    {
        public string Code { get; private set; }
        public string Message { get; private set; }

        public NotFound(string message)
        {
            Code = "NotFound";
            Message = message;
        }
    }
}