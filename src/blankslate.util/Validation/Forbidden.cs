﻿namespace blankslate.util.Validation
{
    public class Forbidden : IValidationMessage
    {
        public string Code { get; private set; }
        public string Message { get; private set; }

        public Forbidden(string message)
        {
            Code = "Forbidden";
            Message = message;
        }
    }
}