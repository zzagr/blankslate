﻿using System;

namespace blankslate.util.Validation
{
    public class ServerError : IValidationMessage
    {
        public string Code { get; private set; }
        public string Message { get; private set; }

        public ServerError(Exception ex)
        {
            Code = "InternalServerError";
            Message = ex.ShowAllMessages();
        }
    }
}