﻿namespace blankslate.util.Configuration
{
    public interface IAppSettings
    {
        string Get(string key);
        string GetConnectionString(string key);
        T GetSection<T>() where T : class, IConfigSection;
        IApplicationDetails GetApplicationDetails();
    }
}