﻿namespace blankslate.util.Configuration
{
    public interface IApplicationDetails : IConfigSection
    {
        string Environment { get; set; }
        string Name { get; set; }
        IEnvironmentConfiguration Configuration { get;  }
        IGoogleAnalytics GoogleAnalytics { get; }
        bool DeveloperMode { get; }
    }
}