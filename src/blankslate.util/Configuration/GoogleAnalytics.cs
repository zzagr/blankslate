﻿using System.Configuration;

namespace blankslate.util.Configuration
{
    public class GoogleAnalytics : ConfigurationElement, IGoogleAnalytics
    {
        private const string AnalyticsIdProperty = "AnalyticsId";
        private const string SiteVerificationProperty = "SiteVerification";

        [ConfigurationProperty(AnalyticsIdProperty, IsRequired = false)]
        public string AnalyticsId
        {
            get { return (string) this[AnalyticsIdProperty]; }
            set { this[AnalyticsIdProperty] = value; }
        }

        [ConfigurationProperty(SiteVerificationProperty, IsRequired = false)]
        public string SiteVerification
        {
            get { return (string) this[SiteVerificationProperty]; }
            set { this[SiteVerificationProperty] = value; }
        }
    }
}