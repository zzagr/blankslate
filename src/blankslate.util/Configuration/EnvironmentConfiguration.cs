﻿using System.Configuration;

namespace blankslate.util.Configuration
{
    public class EnvironmentConfiguration : ConfigurationElement, IEnvironmentConfiguration
    {
        private const string LogLevelProperty = "VerboseDebug";
        private const string NoDbProperty = "NoDb";

        [ConfigurationProperty(LogLevelProperty, IsRequired = false)]
        public bool VerboseDebug
        {
            get { return this[LogLevelProperty].ToNullableBoolean() ?? false; }
            set { this[LogLevelProperty] = value; }
        }

        [ConfigurationProperty(NoDbProperty, IsRequired = false)]
        public bool NoDb
        {
            get { return this[NoDbProperty].ToNullableBoolean() ?? false; }
            set { this[NoDbProperty] = value; }
        }
    }
}