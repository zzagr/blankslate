﻿using System.Web.Configuration;

namespace blankslate.util.Configuration
{
    public class WebSettings : IWebSettings
    {
        public string Get(string key)
        {
            return WebConfigurationManager.AppSettings[key];
        }

        public string GetConnectionString(string key)
        {
            return WebConfigurationManager.ConnectionStrings[key].ConnectionString;
        }

        public T GetSection<T>() where T : class, IConfigSection 
        {
            return WebConfigurationManager.GetSection(typeof (T).Name) as T;
        }

        public IApplicationDetails GetApplicationDetails()
        {
            return GetSection<ApplicationDetails>();
        }
    }
}