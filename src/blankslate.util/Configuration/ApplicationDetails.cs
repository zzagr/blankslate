﻿using System.Configuration;

namespace blankslate.util.Configuration
{
    public class ApplicationDetails : ConfigurationSection, IApplicationDetails
    {
        private const string EnvironmentProperty = "Environment";
        private const string NameProperty = "Name";
        private const string ConfigurationProperty = "Configuration";
        private const string GoogleAnalyticsProperty = "GoogleAnalytics";

        [ConfigurationProperty(ConfigurationProperty, IsRequired = true)]
        public EnvironmentConfiguration EnvironmentConfiguration
        {
            get { return (EnvironmentConfiguration) base[ConfigurationProperty]; }
        }

        [ConfigurationProperty(GoogleAnalyticsProperty)]
        public GoogleAnalytics GoogleAnalyticsConfiguration
        {
            get { return (GoogleAnalytics)base[GoogleAnalyticsProperty]; }
        }

        [ConfigurationProperty(EnvironmentProperty, IsRequired = true)]
        public string Environment
        {
            get { return (string) this[EnvironmentProperty]; }
            set { this[EnvironmentProperty] = value; }
        }

        [ConfigurationProperty(NameProperty, IsRequired = true)]
        public string Name
        {
            get { return (string) this[NameProperty]; }
            set { this[NameProperty] = value; }
        }

        public IEnvironmentConfiguration Configuration
        {
            get { return EnvironmentConfiguration; }
        }

        public IGoogleAnalytics GoogleAnalytics
        {
            get { return GoogleAnalyticsConfiguration; }
        }

        public bool DeveloperMode
        {
            get { return Environment.ToLower() == "dev"; }
        }
    }
}