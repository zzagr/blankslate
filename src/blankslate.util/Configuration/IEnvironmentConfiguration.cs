﻿namespace blankslate.util.Configuration
{
    public interface IEnvironmentConfiguration
    {
        bool VerboseDebug { get; set; }
        bool NoDb { get; set; }
    }
}