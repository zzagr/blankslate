﻿namespace blankslate.util.Configuration
{
    public interface IGoogleAnalytics
    {
        string AnalyticsId { get; set; }
        string SiteVerification { get; set; }
    }
}