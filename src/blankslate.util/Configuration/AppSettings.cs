﻿using System.Configuration;

namespace blankslate.util.Configuration
{
    public class AppSettings : IAppSettings
    {
        public string Get(string key)
        {
            return ConfigurationManager.AppSettings[key];
        }

        public string GetConnectionString(string key)
        {
            return ConfigurationManager.ConnectionStrings[key].ConnectionString;
        }

        public T GetSection<T>() where T : class, IConfigSection 
        {
            return ConfigurationManager.GetSection(typeof (T).Name) as T;
        }

        public IApplicationDetails GetApplicationDetails()
        {
            return GetSection<ApplicationDetails>();
        }
    }
}