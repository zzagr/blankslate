﻿using System;
using System.Collections.Generic;

namespace blankslate.util.Dynamic
{
    public class CustomObjectComparer<TPropertyName> : IComparer<CustomObject> where TPropertyName : IComparable
    {
        private readonly string _propertyName;

        public CustomObjectComparer(string propertyName)
        {
            _propertyName = propertyName;
        }

        public int Compare(CustomObject x, CustomObject y)
        {
            return ((TPropertyName) x[_propertyName]).CompareTo((TPropertyName) y[_propertyName]);
        }
    }
}