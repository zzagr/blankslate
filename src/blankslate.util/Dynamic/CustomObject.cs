﻿using System.Collections.Generic;

namespace blankslate.util.Dynamic
{
    public class CustomObject : Dictionary<string, object>
    {
        public new object this[string name]
        {
            get { return ContainsKey(name) ? base[name] : null; }
            set { base[name] = value; }
        }
    }
}