﻿using System.Collections.Generic;
using AutoMapper;

namespace blankslate.util.Mapping
{
    public abstract class BaseMapper<TSource, TDestination> : IMapper<TSource, TDestination>
        where TSource : class
        where TDestination : class
    {
        public TDestination Map(TSource source)
        {
            return Mapper.Map<TDestination>(source);
        }

        public IEnumerable<TDestination> Map(IEnumerable<TSource> source)
        {
            return source == null ? new TDestination[0] : Mapper.Map<IEnumerable<TDestination>>(source);
        }
    }
}