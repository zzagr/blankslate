﻿using System.Collections.Generic;

namespace blankslate.util.Mapping
{
    public interface IMapper<in TSource, out TDestination>
        where TSource : class
        where TDestination : class
    {
        TDestination Map(TSource source);
        IEnumerable<TDestination> Map(IEnumerable<TSource> source);
    }
}