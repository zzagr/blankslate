﻿using System;
using System.Text;

namespace blankslate.util
{
    public static class ExceptionExtensions
    {
        public static string ShowAllMessages(this Exception ex)
        {
            return AssembleExceptions(ex);
        }

        public static string AssembleExceptions(Exception ex)
        {
            var builder = new StringBuilder();
            AssembleExceptions(builder, ex);

            return builder.ToString();
        }

        private static void AssembleExceptions(StringBuilder builder, Exception ex)
        {
            while (true)
            {
                if (ex == null) return;
                if (string.IsNullOrEmpty(ex.Message)) return;
                if (string.IsNullOrEmpty(ex.StackTrace)) return;
                builder.AppendLine("{0} {1}".With(ex.Message, ex.StackTrace));
                if (ex.InnerException == null) return;

                ex = ex.InnerException;
            }
        }
    }
}