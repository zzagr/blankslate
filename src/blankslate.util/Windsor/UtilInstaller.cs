﻿using Castle.DynamicProxy;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using blankslate.util.Cache;
using blankslate.util.Logging;

namespace blankslate.util.Windsor
{
    public class UtilInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Component.For<IMemCache>().ImplementedBy<MemCache>());
            container.Register(Component.For<ILogger>().ImplementedBy<ConsoleLogger>());
            container.Register(Component.For<IInterceptor>().ImplementedBy<LoggingInterceptor>().LifestyleTransient());
        }
    }
}