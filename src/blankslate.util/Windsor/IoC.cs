﻿namespace blankslate.util.Windsor
{
    public class IoC
    {
        private static readonly IoC Instance = new IoC(new ResolveNothing());

        private IResolve _container;

        public IoC(IResolve container)
        {
            _container = container;
        }

        public static IResolve Resolve
        {
            get { return Instance._container; }
            set { Instance._container = value; }
        }

        public static void Reset()
        {
            Instance._container = new ResolveNothing();
        }

        private class ResolveNothing : IResolve
        {
            public T Component<T>()
            {
                return default(T);
            }

            public T Component<T>(string key)
            {
                return default(T);
            }
        }
    }
}