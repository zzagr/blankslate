﻿using System;
using System.Linq;
using Castle.DynamicProxy;
using blankslate.util.Configuration;
using blankslate.util.Logging;

namespace blankslate.util.Windsor
{
    public class LoggingInterceptor : IInterceptor
    {
        private readonly ILogger _logger;
        private readonly IAppSettings _settings;

        public LoggingInterceptor(ILogger logger, IAppSettings settings)
        {
            _logger = logger;
            _settings = settings;
        }

        public void Intercept(IInvocation invocation)
        {
            var verboseDebug = _settings.GetApplicationDetails().Configuration.VerboseDebug;
            if (verboseDebug)
                _logger.Debug("Method {0}.{1} called with arguments {2}", invocation.TargetType.Name, invocation.Method.Name,
                    invocation.Arguments.FirstOrDefault());
            try
            {
                invocation.Proceed();
            }
            catch (Exception e)
            {
                _logger.Error(e);
                throw;
            }
        }
    }
}