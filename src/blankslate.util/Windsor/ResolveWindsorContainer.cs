﻿using Castle.Windsor;

namespace blankslate.util.Windsor
{
    public class ResolveWindsorContainer : IResolve
    {
        private readonly IWindsorContainer _container;

        public ResolveWindsorContainer(IWindsorContainer container)
        {
            _container = container;
        }

        public T Component<T>()
        {
            return _container.Resolve<T>();
        }

        public T Component<T>(string key)
        {
            return _container.Resolve<T>(key);
        }

        public T Component<T>(object argsAsAnonymousType)
        {
            return _container.Resolve<T>(argsAsAnonymousType);
        }

        public T Component<T>(string key, object argsAsAnonymousType)
        {
            return _container.Resolve<T>(key, argsAsAnonymousType);
        }
    }
}