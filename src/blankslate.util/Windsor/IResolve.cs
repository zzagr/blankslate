﻿namespace blankslate.util.Windsor
{
    public interface IResolve
    {
        T Component<T>();
        T Component<T>(string key);
    }
}