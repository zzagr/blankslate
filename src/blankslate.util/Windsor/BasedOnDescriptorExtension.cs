﻿using Castle.Core;
using Castle.MicroKernel.Registration;

namespace blankslate.util.Windsor
{
    public static class BasedOnDescriptorExtension
    {
        public static BasedOnDescriptor WithLoggingInterceptor(this BasedOnDescriptor descriptor)
        {
            //If you don't call "ComponentRegistration<>.Anywhere" on component registration, interceptors won't get applied
            return descriptor.Configure(c => { var registration = c.Interceptors(InterceptorReference.ForType<LoggingInterceptor>()).Anywhere; });
        }
    }
}