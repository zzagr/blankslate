﻿using System;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using Newtonsoft.Json;

namespace blankslate.util
{
    public static class StringExtensions
    {
        public static string With(this string format, params object[] args)
        {
            if (string.IsNullOrEmpty(format))
                ThrowException();
            try
            {
                return args != null ? string.Format(format, args) : string.Format("{0}".With(format));
            }
            catch
            {
                throw new FormatException("Format string provided is invalid.");
            }
        }

        public static T ConvertTo<T>(this string json, JsonSerializerSettings settings = null)
        {
            return settings == null
                ? JsonConvert.DeserializeObject<T>(json)
                : JsonConvert.DeserializeObject<T>(json, settings);
        }

        public static Stream ToStream(this string item)
        {
            var outBytes = Encoding.ASCII.GetBytes(item);
            var stream = new MemoryStream();
            stream.Write(outBytes, 0, outBytes.Length);
            stream.Position = 0;

            return stream;
        }

        public static string Wordify(this string pascalCaseString)
        {
            var regex = new Regex("(?<=[a-z])(?<x>[A-Z])|(?<=.)(?<x>[A-Z])(?=[a-z])");
            return regex.Replace(pascalCaseString, " ${x}");
        }

        private static void ThrowException()
        {
            var stackTrace = new StackTrace();
            var frame = stackTrace.GetFrame(2);
            var method = frame.GetMethod();
            var className = method.DeclaringType == null ? string.Empty : method.DeclaringType.Name;
            throw new ApplicationException("Error formatting string. Input string is null at {0}.{1}(...)".With(className, method.Name));
        }
    }
}