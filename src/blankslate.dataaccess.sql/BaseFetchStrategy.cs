﻿using blankslate.util.Windsor;
using NHibernate;

namespace blankslate.dataaccess.sql
{
    public abstract class BaseFetchStrategy
    {
        public ISession Session
        {
            get { return IoC.Resolve.Component<ISession>(); }
        }
    }
}