﻿using System.Web.Mvc;
using blankslate.tasks;

namespace blankslate.web.Controllers
{
    public class DefaultController : Controller
    {
        private IPingTask _task;

        public DefaultController(IPingTask task)
        {
            _task = task;
        }

        public ActionResult Index()
        {
            return View();
        }
    }
}