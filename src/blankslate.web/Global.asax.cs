﻿using System;
using System.Web;
using blankslate.util.Logging;
using blankslate.util.Windsor;
using blankslate.Web;

namespace blankslate.web
{
    public class Global : HttpApplication
    {
        protected void Application_Start()
        {
            StartupTask.Start();
        }

        public override void Dispose()
        {
            StartupTask.Shutdown();
            base.Dispose();
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            var logger = IoC.Resolve.Component<ILogger>();

            logger.Fatal(Server.GetLastError());
        }
    }
}