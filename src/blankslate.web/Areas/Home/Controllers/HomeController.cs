﻿using System.Web.Mvc;

namespace blankslate.web.Areas.Home.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}