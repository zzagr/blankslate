﻿using System.Net.Http;
using System.Web.Http;
using blankslate.tasks;
using blankslate.web.util.Controller;

namespace blankslate.web.Areas.Ping.Controllers
{
    [RoutePrefix("Ping")]
    public class PingController : BaseApiController
    {
        private readonly IPingTask _task;

        public PingController(IPingTask task)
        {
            _task = task;
        }

        [Route("")]
        [HttpGet]
        public HttpResponseMessage CheckIfEverythingWorks()
        {
            return CreateHttpResponse<bool>(Request, _task.Ping);
        }
    }
}