(function() {
    'use strict';

    angular
        .module('app.layout')
        .directive('almaDashboardHeader', almaDashboardHeader);

    function almaDashboardHeader () {
        // Usage:
        //<alma-dashboard-header></alma-dashboard-header>
        // Creates:
        //generated header html. see templateUrl
        var directive = {
        	templateUrl: '/Scripts/src/app/js/layout/views/poc/alma-dashboard-header.html',
            restrict: 'E'
        };
        return directive;

        
    }
})();