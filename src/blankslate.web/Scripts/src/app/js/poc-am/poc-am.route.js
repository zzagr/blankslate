(function() {
    'use strict';

    angular
        .module('app.poc-am')
        .config(RegisterRoutes);

    RegisterRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];

    function RegisterRoutes($stateProvider, $urlRouterProvider) {
        var parentUri = '/poc-am';

        $stateProvider
            .state('poc-am-home', {
                url: parentUri + '/dashboard',
                templateUrl: '/Scripts/src/app/js/poc-am/views/dashboard.html',
                controller: 'AmDashboardController',
                controllerAs: 'vm'
            });
    }

})();
