(function() {
    'use strict';

    angular
        .module('app.poc-am', [
            'app.core'
        ]);
})();