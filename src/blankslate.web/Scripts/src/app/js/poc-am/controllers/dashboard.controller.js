(function() {
    'use strict';

    angular
        .module('app.poc-am')
        .controller('AmDashboardController', AmDashboardController);

    AmDashboardController.$inject = ['$q', 'SurveyResponseTask', 'noviSurveyId', 'logger', 'progressBar'];

    /* @ngInject */
    function AmDashboardController($q, SurveyResponseTask, noviSurveyId, logger, progressBar) {
        var vm = this;
        vm.title = 'AmDashboardController';

        activate();

        ////////////////

        function activate() {
            // var promises = [
            //     getResponseSummary('P01Q1', 'Compoany Size'),
            //     getResponseSummary('P01Q2', 'Industries'),
            //     getResponseSummary('P01Q3', 'Regions'),
            //     getResponseSummary('P02Q1', 'Attracting Workers'),
            //     getResponseSummary('P03Q1', 'Skills to Attract'),
            //     getResponseSummary('P04Q1', 'Areas to Attract'),
            //     getResponseSummary('P05Q1', 'Incentives'),
            //     getResponseSummary('P06Q1', 'Government Resources'),
            //     getResponseSummary('P07Q1', 'Time to Fill Position'),
            //     getResponseSummary('P08Q1', 'Employee Retention'),
            //     getResponseSummary('P09Q1', 'Perks'),
            //     getResponseSummary('P10Q1', 'Training Opportunities')
            // ];

            progressBar.initialize(3);

            // return $q.all(promises).then(function() {});


        }

        function getResponseSummary(questionKey, questionType) {
            var summary = SurveyResponseTask.getResponseSummary(noviSurveyId, questionKey);

            return summary.then(function(data) {
                progressBar.increment(questionType);
            });
        }
    }
})();
