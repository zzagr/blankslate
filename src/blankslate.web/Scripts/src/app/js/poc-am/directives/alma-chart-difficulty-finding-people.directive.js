(function() {
    'use strict';

    angular
        .module('app.poc-am')
        .directive('almaChartDifficultyFindingPeople', almaChartDifficultyFindingPeople);

    almaChartDifficultyFindingPeople.$inject = [];

    /* @ngInject */
    function almaChartDifficultyFindingPeople() {
        // Usage:
        //
        // Creates:
        //
        var directive = {
            bindToController: true,
            templateUrl: '/Scripts/src/app/js/poc-am/views/directives/alma-chart-difficulty-finding-people.html',
            controller: ChartDifficultyFindingPeopleController,
            controllerAs: 'vm',
            link: link,
            restrict: 'E',
            scope: {}
        };
        return directive;

        function link(scope, element, attrs) {}
    }

    ChartDifficultyFindingPeopleController.$inject = ['$q', 'SurveyResponseTask', 'progressBar', 'noviSurveyId']

    function ChartDifficultyFindingPeopleController($q, SurveyResponseTask, progressBar, noviSurveyId) {

        activate();

        ////////////////

        function activate() {
            var promises = [
                getResponseSummary('P02Q1', 'Attracting Workers')
            ];

            return $q.all(promises).then(function() {});
        }



        function getResponseSummary(questionKey, questionType) {
            var summary = SurveyResponseTask.getResponseSummary(noviSurveyId, questionKey);

            return summary.then(function(data) {
                progressBar.increment(questionType);
                AmCharts.ready(initializeChart(data));
            });
        }

        function initializeChart(chartData) {
            // PIE CHART
            var chart = new AmCharts.AmPieChart();
            console.log(chartData);
            // title of the chart
            chart.addTitle('Difficulty Finding People', 16);

            chart.dataProvider = chartData;
            chart.theme = 'dark',
            chart.titleField = 'RecordedResponse';
            chart.valueField = 'ResponseCount';
            chart.sequencedAnimation = true;
            chart.startEffect = 'elastic';
            chart.innerRadius = '50%';
            chart.startDuration = 2;
            chart.labelRadius = 15;
            chart.balloonText = '[[title]]<br><span style="font-size:14px"><b>[[value]]</b> ([[percents]]%)</span>';
            // the following two lines makes the chart 3D
            chart.depth3D = 15;
            chart.angle = 15;

            // WRITE
            chart.write('diffculty-finding-people-chart');

        }


    }
})();
