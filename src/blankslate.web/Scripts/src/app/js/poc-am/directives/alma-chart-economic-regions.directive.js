(function() {
    'use strict';

    angular
        .module('app.poc-am')
        .directive('almaChartEconomicRegions', almaChartEconomicRegions);

    almaChartEconomicRegions.$inject = [];

    /* @ngInject */
    function almaChartEconomicRegions() {
        // Usage:
        //
        // Creates:
        //
        var directive = {
            bindToController: true,
            templateUrl: '/Scripts/src/app/js/poc-am/views/directives/alma-chart-economic-regions.html',
            controller: ChartEconomicRegionsController,
            controllerAs: 'vm',
            link: link,
            restrict: 'E',
            scope: {}
        };
        return directive;

        function link(scope, element, attrs) {}
    }

    ChartEconomicRegionsController.$inject = ['$q', 'SurveyResponseTask', 'progressBar', 'noviSurveyId', 'logger'];
        /* @ngInject */
    function ChartEconomicRegionsController($q, SurveyResponseTask, progressBar, noviSurveyId, logger) {
        activate();

        ////////////////

        function activate() {
            var promises = [
                getResponseSummary('P01Q3', 'Economic Regions')
            ];

            return $q.all(promises).then(function() {});
        }



        function getResponseSummary(questionKey, questionType) {
            var summary = SurveyResponseTask.getResponseSummary(noviSurveyId, questionKey);

            return summary.then(function(data) {
                progressBar.increment(questionType);
                AmCharts.ready(
                    initializeChart(data)
                );
            });
        }

        function initializeChart(chartData) {

            var chart = new AmCharts.AmSerialChart();
            chart.dataProvider = chartData;
            chart.categoryField = "RecordedResponse";
            // this single line makes the chart a bar chart,
            // try to set it to false - your bars will turn to columns
            chart.rotate = true;
            // the following two lines makes chart 3D
            chart.depth3D = 20;
            chart.angle = 30;
            chart.addListener("clickGraphItem", handleClick);

            // AXES
            // Category
            var categoryAxis = chart.categoryAxis;
            categoryAxis.gridPosition = "start";
            categoryAxis.axisColor = "#DADADA";
            categoryAxis.fillAlpha = 1;
            categoryAxis.gridAlpha = 0;
            categoryAxis.fillColor = "#FAFAFA";

            // value
            var valueAxis = new AmCharts.ValueAxis();
            valueAxis.axisColor = "#DADADA";
            valueAxis.title = "Economic Regions";
            valueAxis.gridAlpha = 0.1;
            chart.addValueAxis(valueAxis);

            // GRAPH
            var graph = new AmCharts.AmGraph();
            graph.title = "Income";
            graph.valueField = "ResponseCount";
            graph.type = "column";
            graph.balloonText = "Income in [[category]]:[[value]]";
            graph.lineAlpha = 0;
            graph.fillColors = "#bf1c25";
            graph.fillAlphas = 1;
            chart.addGraph(graph);

            chart.creditsPosition = "top-right";

            // WRITE
            chart.write('economic-regions-chart');

        }

        function handleClick(event) {
            logger.info(event.item.category + ": " + event.item.values.value, event.item);
        }
    }
})();
