(function() {
    'use strict';

    angular
        .module('app.poc-am')
        .directive('almaChartDifficultyKeepingPeople', almaChartDifficultyKeepingPeople);

    almaChartDifficultyKeepingPeople.$inject = [];

    /* @ngInject */
    function almaChartDifficultyKeepingPeople() {
        // Usage:
        //
        // Creates:
        //
        var directive = {
            bindToController: true,
            templateUrl: '/Scripts/src/app/js/poc-am/views/directives/alma-chart-difficulty-keeping-people.html',
            controller: ChartDifficultyKeepingPeopleController,
            controllerAs: 'vm',
            link: link,
            restrict: 'E',
            scope: {}
        };
        return directive;

        function link(scope, element, attrs) {}
    }

    ChartDifficultyKeepingPeopleController.$inject = ['$q', 'SurveyResponseTask', 'progressBar', 'noviSurveyId']

    function ChartDifficultyKeepingPeopleController($q, SurveyResponseTask, progressBar, noviSurveyId) {

        activate();

        ////////////////

        function activate() {
            var promises = [
                getResponseSummary('P08Q1', 'Employee Retention')
            ];

            return $q.all(promises).then(function() {});
        }



        function getResponseSummary(questionKey, questionType) {
            var summary = SurveyResponseTask.getResponseSummary(noviSurveyId, questionKey);

            return summary.then(function(data) {
                progressBar.increment(questionType);
                AmCharts.ready(initializeChart(data));
            });
        }

        function initializeChart(chartData) {
            // PIE CHART
            var chart = new AmCharts.AmPieChart();
            
            // title of the chart
            chart.addTitle('Difficulty Keeping People', 16);

            chart.dataProvider = chartData;
            chart.theme = 'dark',
            chart.titleField = 'RecordedResponse';
            chart.valueField = 'ResponseCount';
            chart.sequencedAnimation = true;
            chart.startEffect = 'elastic';
            chart.innerRadius = '30%';
            chart.startDuration = 2;
            chart.labelRadius = 15;
            chart.balloonText = '[[title]]<br><span style="font-size:14px"><b>[[value]]</b> ([[percents]]%)</span>';
            // the following two lines makes the chart 3D
            chart.depth3D = 10;
            chart.angle = 15;

            // WRITE
            chart.write('diffculty-keeping-people-chart');

        }


    }
})();
