(function() {
    'use strict';

    angular
        .module('app.poc-am')
        .service('SurveyResponseTask', SurveyResponseTask);

    SurveyResponseTask.$inject = ['$http', 'logger'];

    function SurveyResponseTask($http, logger) {
        return {
            getResponseSummary: getResponseSummary
        };

        function getResponseSummary(surveyKey, questionKey) {
            return $http.get('/api/poc/responsesummaries/' + surveyKey + '?questionKey=' + questionKey)
                .then(success)
                .catch(fail);

            function success(response) {
                return response.data;
            }

            function fail(error) {
                logger.error(error);
                return [];
            }
        }
    }
})();
