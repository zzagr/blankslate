//http://stackoverflow.com/questions/20745761/what-is-the-angular-ui-router-lifecycle-for-debugging-silent-errors

(function() {
    'use strict';

    angular
        .module('app.core')
        .service('routeLogger', routeLogger);

    routeLogger.$inject = ['$rootScope', 'logger'];

    function routeLogger($rootScope, logger) {
        var handler = {
            active: false
        };
        handler.toggle = function() {
            handler.active = !handler.active;
        };
        $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
            if (!handler.active) return;
            logger.info("$stateChangeStart --- event, toState, toParams, fromState, fromParams", arguments);
        });
        $rootScope.$on('$stateChangeError', function(event, toState, toParams, fromState, fromParams, error) {
            if (!handler.active) return;
            logger.info("$stateChangeError --- event, toState, toParams, fromState, fromParams, error", arguments);
        });
        $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
            if (!handler.active) return;
            logger.info("$stateChangeSuccess --- event, toState, toParams, fromState, fromParams", arguments);
        });
        $rootScope.$on('$viewContentLoading', function(event, viewConfig) {
            if (!handler.active) return;
            logger.info("$viewContentLoading --- event, viewConfig", arguments);
        });
        $rootScope.$on('$viewContentLoaded', function(event) {
            if (!handler.active) return;
            logger.info("$viewContentLoaded --- event", arguments);
        });
        $rootScope.$on('$stateNotFound', function(event, unfoundState, fromState, fromParams) {
            if (!handler.active) return;
            logger.info("$stateNotFound --- event, unfoundState, fromState, fromParams", arguments);
        });
        $rootScope.$on('$routeChangeError', function(event, current, previous, rejection) {
            if (!handler.active) return;
            logger.info("$stateNotFound --- event, current, previous, rejection", arguments);
        });
        $rootScope.$on('$routeChangeStart', function(event, next, current) {
            if (!handler.active) return;
            logger.info("$stateNotFound --- event, next, current", arguments);
        });
        $rootScope.$on('$routeChangeSuccess', function(event, current, previous) {
            if (!handler.active) return;
            logger.info("$stateNotFound --- event, current, previous", arguments);
        });
        $rootScope.$on('$routeUpdate', function(event) {
            if (!handler.active) return;
            logger.info("$routeUpdate --- event", arguments);
        });

        return handler;
    }
})();
