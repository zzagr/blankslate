(function() {
    'use strict';

    angular
        .module('app.core')
        .service('progressBar', progressBar);

    progressBar.$inject = ['$rootScope', 'logger'];

    /* @ngInject */
    function progressBar($rootScope, logger) {
        return {
            initialize: initialize,
            increment: increment
        };

        function initialize(max) {
            $rootScope.max = max;
            $rootScope.progressBarStatus = 0;
            $rootScope.responseCount = 0;
        };

        function increment(itemToLoad) {
        	var loadingMessage = 'Loading ' + itemToLoad + '...';
        	logger.info(loadingMessage);
        	$rootScope.progressBarMessasge = loadingMessage;
            $rootScope.responseCount = $rootScope.responseCount + 1;
            $rootScope.progressBarStatus = ($rootScope.responseCount / $rootScope.max) * 100;
        };

    }
})();
