(function() {
    "use strict";
    
    var core = angular.module("app.core");

    core.run(InitializeRouting);
    InitializeRouting.$inject = ['$state'];
    function InitializeRouting($state){
    	//This is a workaround. See: https://github.com/angular-ui/ui-router/issues/679
    	//ui-view inside an ng-include won't trigger routing
    }
    
})();