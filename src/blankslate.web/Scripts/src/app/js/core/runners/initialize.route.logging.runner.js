(function() {
    "use strict";
    
    var core = angular.module("app.core");
  
    core.run(ShowRoutingEvents);
    ShowRoutingEvents.$inject = ['routeLogger','env'];
    function ShowRoutingEvents(routeLogger,env){
    	if(env && env !== 'dev') return;
    	routeLogger.active = true;
    }
    
})();