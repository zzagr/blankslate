(function() {
    'use strict';

    angular
        .module('app.core')
        .config(RegisterRoutes);

    RegisterRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];

    function RegisterRoutes($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise('/poc/dashboard');
    }

})();
