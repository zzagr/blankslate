(function() {
    'use strict';

    angular
        .module('app.core')
        .directive('goaProgressBar', goaProgressBar);

    function goaProgressBar() {
        
        var directive = {
            restrict: 'E',
            template: '<progressbar class="progress-striped active" value="progressBarStatus" type="success">Loading {{progressBarMessasge}}...</progressbar>'
        };

        return directive;

    }


})();
