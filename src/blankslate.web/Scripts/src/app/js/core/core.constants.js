﻿(function() {
    "use strict";

    var pathArray = window.location.pathname.split("/");
    angular
        .module("app.core")
        .constant("toastr", toastr)
        .constant("baseUri", "/" + pathArray[1] + "/");
})();
