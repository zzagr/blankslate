(function() {
    "use strict";
    
    var core = angular.module("app.core");
    core.config(initializeToastr);
    initializeToastr.$inject = ["toastr"];
    function initializeToastr(toastr) {
        toastr.options.timeOut = 4000;
        toastr.options.positionClass = "toast-bottom-right";
    }
    
})();