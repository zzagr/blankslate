(function() {
    'use strict';

    angular
        .module('app.poc')
        .directive('almaBusinessInfoModal', almaBusinessInfoModal);

    /* @ngInject */
    function almaBusinessInfoModal() {
       var directive = {
            templateUrl: '/Scripts/src/app/js/poc/views/directives/alma-business-info-modal.html'
       };

        return directive;

    }


})();
