(function() {
    'use strict';

    angular
        .module('app.poc')
        .directive('almaEmployeeRecruitmentModal', almaEmployeeRecruitmentModal);

    /* @ngInject */
    function almaEmployeeRecruitmentModal() {
       var directive = {
            templateUrl: '/Scripts/src/app/js/poc/views/directives/alma-employee-recruitment-modal.html'
       };

        return directive;

    }


})();
