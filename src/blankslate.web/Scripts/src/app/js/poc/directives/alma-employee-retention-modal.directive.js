(function() {
    'use strict';

    angular
        .module('app.poc')
        .directive('almaEmployeeRetentionModal', almaEmployeeRetentionModal);

    /* @ngInject */
    function almaEmployeeRetentionModal() {
       var directive = {
            templateUrl: '/Scripts/src/app/js/poc/views/directives/alma-employee-retention-modal.html'
       };

        return directive;

    }


})();
