(function() {
    'use strict';

    angular
        .module('app.poc')
        .directive('almaNoviSurvey', almaNoviSurvey);

    almaNoviSurvey.$inject = ['noviSurveyUri', 'noviSurveyId'];

    /* @ngInject */
    function almaNoviSurvey(noviSurveyUri, noviSurveyId) {
        // Usage:
        //<alma-novi-survey></alma-novi-survey>
        // Creates:
        //<iframe width='100%' height='650' frameborder='0' src='http://vm-internet-6p/novi/TakeSurvey.aspx?s=53'></iframe>
        var embedUri = noviSurveyUri + '/TakeSurvey.aspx?s=' + noviSurveyId;
        var directive = {
            restrict: 'E',
            template: '<iframe width=\'100%\' height=\'650\' frameborder=\'0\' src=\'' + embedUri + '\'></iframe>'
        };

        return directive;

    }


})();
