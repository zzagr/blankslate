(function() {
    'use strict';

    angular
        .module('app.poc')
        .directive('almaRespondentsCounter', almaRespondentsCounter);

    /* @ngInject */
    function almaRespondentsCounter() {
       var directive = {
            templateUrl: '/Scripts/src/app/js/poc/views/directives/alma-respondents-counter.html'
       };

        return directive;

    }


})();
