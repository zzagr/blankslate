(function() {
    'use strict';

    angular
        .module('app.poc')
        .directive('almaGovernmentResourcesModal', almaGovernmentResourcesModal);

    /* @ngInject */
    function almaGovernmentResourcesModal() {
       var directive = {
            templateUrl: '/Scripts/src/app/js/poc/views/directives/alma-government-resources-modal.html'
       };

        return directive;

    }


})();
