(function() {
    'use strict';

    angular
        .module('app.poc')
        .config(RegisterRoutes);

    RegisterRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];

    function RegisterRoutes($stateProvider, $urlRouterProvider) {
        var parentUri = '/poc';

        $urlRouterProvider.when('/', parentUri + '/dashboard');

        $stateProvider
            .state('poc-home', {
                url: parentUri + '/dashboard',
                templateUrl: '/Scripts/src/app/js/poc/views/dashboard.html',
                controller: 'DashboardController',
                controllerAs: 'vm'
            })
            .state('poc-surveys', {
                url: parentUri + '/surveys',
                templateUrl: '/Scripts/src/app/js/poc/views/survey.html',
                controller: 'SurveyController',
                controllerAs: 'vm'
            })
            .state('poc-surveyDetails', {
                url: parentUri + '/surveys/:surveyKey',
                templateUrl: '/Scripts/src/app/js/poc/views/survey.details.html',
                controller: 'SurveyDetailsController',
                controllerAs: 'vm'
            })
            .state('poc-novi', {
                url: parentUri + '/novi',
                templateUrl: '/Scripts/src/app/js/poc/views/novi.survey.html'
            });
    }

})();
