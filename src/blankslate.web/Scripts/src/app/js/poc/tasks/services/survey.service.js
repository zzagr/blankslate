﻿(function() {
    'use strict';

    angular
        .module('app.poc')
        .service('SurveyService', SurveyService);

    SurveyService.$inject = ['$http'];

    function SurveyService($http) {
        return {
            getAllSurveyKeys: getAllSurveyKeys
        };

        function getAllSurveyKeys() {
            return $http.get('/api/poc/Surveys')
                .then(success)
                .catch(fail);

            function success(response) {
                return response.data;
            }

            function fail(error) {
                console.log('fail' + error);
                return [];
            }
        }


    }
})();
