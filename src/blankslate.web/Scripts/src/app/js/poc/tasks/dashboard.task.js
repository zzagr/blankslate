(function() {
    'use strict';

    angular
        .module('app.poc')
        .factory('DashboardTask', DashboardTask);

    DashboardTask.$inject = ['SurveyResponseService', 'noviSurveyId', 'logger'];

    /* @ngInject */
    function DashboardTask(SurveyResponseService, noviSurveyId, logger) {
        var service = {
            initializeCharts: initializeCharts
        };
        return service;

        ////////////////

        function initializeCharts(surveyResponses) {
            var ndx = crossfilter(surveyResponses);
            new ChartsPipeline(dc, ndx).Initialize();

            dc.renderAll();
        }
    }
})();
