(function() {
    'use strict';

    angular
        .module('app.poc')
        .service('SurveyResponseService', SurveyResponseService);

    SurveyResponseService.$inject = ['$http'];

    /* @ngInject */
    function SurveyResponseService($http) {
        return {
            getAllResponses: getAllResponses
        };

        function getAllResponses(surveyKey) {
            return $http.get('/api/poc/SurveyResponses/' + surveyKey)
                .then(success)
                .catch(fail);

            function success(response) {
                return response.data;
            }

            function fail(error) {
                console.log(error);
                return [];
            }
        }

    }
})();
