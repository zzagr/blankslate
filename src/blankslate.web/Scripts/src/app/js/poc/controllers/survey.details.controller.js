(function() {
    'use strict';

    angular
        .module('app.poc')
        .controller('SurveyDetailsController', SurveyDetailsController);

    SurveyDetailsController.$inject = ['$q', '$stateParams', 'SurveyResponseService'];

    function SurveyDetailsController($q, $stateParams, SurveyResponseService) {
        var vm = this;
        vm.title = 'SurveyDetailsController';

        activate();

        ////////////////

        function activate() {
            var promises = [getAllResponses()];
            return $q.all(promises).then(function() {
                console.log('done');
            });
        }

        function getAllResponses() {
            return SurveyResponseService.getAllResponses($stateParams.surveyKey).then(function(data) {
                console.log(data);
                vm.data = data;
            });
        }
    }
})();
