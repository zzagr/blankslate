(function() {
    'use strict';

    angular
        .module('app.poc')
        .controller('SurveyController', SurveyController);

    SurveyController.$inject = ['$q', 'SurveyService', 'logger'];

    /* @ngInject */
    function SurveyController($q, SurveyService, logger) {
        var vm = this;
        vm.title = 'SurveyController';

        activate();

        ////////////////

        function activate() {
        	var promises = [getAllSurveyKeys()];
              return $q.all(promises).then(function() {
                logger.info('done');
            });
        }

        function getAllSurveyKeys(){
            return SurveyService.getAllSurveyKeys().then(function(data){
                vm.surveyKeys = data;
                // return vm.surveyKeys;
            });
        }

    }
})();