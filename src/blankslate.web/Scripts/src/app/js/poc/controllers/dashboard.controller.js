(function() {
    'use strict';

    angular
        .module('app.poc')
        .controller('DashboardController', DashboardController);

    DashboardController.$inject = ['$q', 'SurveyResponseService', 'DashboardTask', 'noviSurveyId', 'logger'];

    /* @ngInject */
    function DashboardController($q, SurveyResponseService, DashboardTask, noviSurveyId, logger) {
        var vm = this;
        vm.title = 'DashboardController';

        activate();



        function activate() {
            var promises = [getAllResponses()];
            return $q.all(promises).then(function() {

            });
        }

        function getAllResponses() {
            return SurveyResponseService.getAllResponses(noviSurveyId).then(function(data) {
                var industries = [];
                var regions = [];
                angular.forEach(data, function(value, key) {
                    industries[value.P01Q2] = 1;
                    regions[value.P01Q3] = 1;
                });
                vm.industriesCount = Object.keys(industries).length;
                vm.respondentsCount = data.length;
                vm.regionsCount = Object.keys(regions).length;
             
                DashboardTask.initializeCharts(data);
                
            });
        }
    }
})();
