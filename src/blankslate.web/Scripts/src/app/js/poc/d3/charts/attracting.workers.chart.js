var AttractingWorkersChart = function(dc, ndx) {
    var attractingWorkersChart = dc.pieChart("#dc-ring-attracting-workers");
    var skillsToAttractId = "#dc-pie-skills-to-attract";
    var skillsToAttractChart = dc.rowChart(skillsToAttractId);
    var skillsToAttractWidth = $(skillsToAttractId).width();
    var attractingWorkersDim = ndx.dimension(dc.pluck('P02Q1'));
    var attractingWorkers = attractingWorkersDim.group();
    
    var all = ndx.groupAll();
    var skillsToAttractDim = ndx.dimension(dc.pluck('P03Q1'));
    var grouping = new SkillsToAttractGrouping();
    var skillsToAttractTotal = skillsToAttractDim.groupAll().reduce(
        grouping.reduceAdd,
        grouping.reduceRemove,
        grouping.reduceInitial).value();

    this.Initialize = function (colorRange) {
        attractingWorkersChart.width(200).height(200)
            .ordinalColors(colorRange)
            .dimension(attractingWorkersDim)
            .group(attractingWorkers)
            .innerRadius(40)
            .label(function (d) {
                if (attractingWorkersChart.hasFilter() && !attractingWorkersChart.hasFilter(d.key)) {
                    return d.key + " (0%)";
                }
                var label = d.key;
                if (all.value()) {
                    label += " (" + Math.floor(d.value / all.value() * 100) + "%)";
                }
                return label;
            })
            .renderTitle(false);

        skillsToAttractChart.width(skillsToAttractWidth).height(340)
            .dimension(skillsToAttractDim)
            .group(skillsToAttractTotal)
            .label(function(d) {
                return d.key + " ["+ d.value + "]";
            });

        skillsToAttractChart.filterHandler(function(dimension, filters) {
            dimension.filter(null);
            if (filters.length === 0)
                dimension.filter(null);
            else
                dimension.filterFunction(function(d) {
                    for (var i = 0; i < d.length; i++) {
                        if (filters.indexOf(d[i]) >= 0) return true;
                    }
                    return false;
                });
            return filters;
        });
    };

    skillsToAttractTotal.all = function() {
        var newObject = [];
        for (var key in this) {
            if (this.hasOwnProperty(key) && key !== "all" && key !== "top") {
                newObject.push({
                    key: key,
                    value: this[key]
                });
            }
        }
        return newObject;
    };

    skillsToAttractTotal.top = function(count) {
        var newObject = this.all();
        newObject.sort(function(a, b) {
            return b.value - a.value
        });
        return newObject.slice(0, count);
    };
};
