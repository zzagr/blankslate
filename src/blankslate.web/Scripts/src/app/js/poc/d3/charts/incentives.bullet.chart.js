var IncentivesBulletChart = function(dataProvider) {
    var chartId = "#nv-bullet-incentives";
    var width = $(chartId).width();
    var height = 75;
    var margin = {
            top: 5,
            right: 20,
            bottom: 20,
            left: 120
        };
    var incentivesChart = nv.models.bulletChart()
        .width(width - margin.right - margin.left)
        .height(height - margin.top - margin.bottom);

    var incentivesVisualization;

    this.Initialize = function(colorRange) {
        initializeBulletChart();

        incentivesVisualization.enter()
            .append("svg")
            .attr("class", "bullet nvd3")
            .attr("width", width)
            .attr("height", height);

        redrawChart();
    };

    this.Redraw = function() {
        initializeBulletChart();
        incentivesVisualization.empty();

        redrawChart();
    }

    function initializeBulletChart() {
        incentivesVisualization = d3.select(chartId)
            .selectAll("svg")
            .data(dataProvider.GetData());
    }

    function redrawChart() {
        incentivesVisualization
            .transition()
            .duration(1000)
            .call(incentivesChart);
    }
};
