var NumberOfEmployeesChart = function(dc, ndx) {
    var chartId = "#dc-pie-number-of-employees";
    var numberOfEmployeesChart = dc.pieChart(chartId);
    var numberOfEmployeesDim = ndx.dimension(dc.pluck('P01Q1'));
    var width = $(chartId).width();
    var countGrouping = new CountGrouping();
    var numberOfEmployeesTotal = numberOfEmployeesDim.group().reduce(
        countGrouping.reduceAdd,
        countGrouping.reduceRemove,
        countGrouping.reduceInitial
    );

    this.Initialize = function (colorRange) {
        numberOfEmployeesChart.width(width).height(width)
            .ordinalColors(colorRange)
            .dimension(numberOfEmployeesDim)
            .group(numberOfEmployeesTotal)

            .keyAccessor(function(p) {
                return p.key;
            })
            .valueAccessor(function(p) {
                return p.value.count;
            })
            .label(function(d) {
                return d.key + " [" + d.value.count + " Respondents]";
            });
    };
}
