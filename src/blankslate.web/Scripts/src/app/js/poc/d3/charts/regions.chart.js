var RegionsChart = function(dc, ndx) {
    var chartId = "#dc-row-regions";
    var regionsChart = dc.rowChart(chartId);
    var regionsDim = ndx.dimension(dc.pluck('P01Q3'));
    var regions = regionsDim.group();
    var width = $(chartId).width();

    this.Initialize = function (colorRange) {
        regionsChart.width(width).height(300)
            .ordinalColors(colorRange)
            .dimension(regionsDim)
            .group(regions)
            .x(d3.scale.linear().domain([0.5, 5.5]))
            .elasticX(true)
            .label(function(d) {
                return d.key + " [" + d.value + "]";
            })
            .legend(dc.legend().x(60).y(10).itemHeight(13).gap(5));
    };
};
