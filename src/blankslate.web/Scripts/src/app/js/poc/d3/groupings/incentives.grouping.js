var IncentivesGrouping = function() {
    this.reduceAdd = function (p, v) {
        if (!v.P05Q1) return p;
        if (v.P05Q1[0] === "") return p; // skip empty values
        v.P05Q1.forEach(function(val, idx) {
            p[val] = (p[val] || 0) + 1; //increment counts
        });
        return p;
    }

    this.reduceRemove = function (p, v) {
        if (!v.P05Q1) return p;
        if (v.P05Q1[0] === "") return p; // skip empty values
        v.P05Q1.forEach(function(val, idx) {
            p[val] = (p[val] || 0) - 1; //decrement counts
        });
        return p;
    }

    this.reduceInitial = function(p, v) {
        return {};
    }
};