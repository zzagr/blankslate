var GovernmentResourcesChart = function(dc, ndx) {
    var grouping = new GovernmentResourcesGrouping();
    var chartId = "#dc-bubble-resources";
    var resourcesChart = dc.rowChart(chartId);
    var resourcesDim = ndx.dimension(dc.pluck("P06Q1"));
    var resourcesTotal = resourcesDim.groupAll().reduce(
        grouping.reduceAdd,
        grouping.reduceRemove,
        grouping.reduceInitial).value();

    var width = $(chartId).width();

    this.Initialize = function (colorRange) {
        resourcesChart.width(width).height(300)
            .ordinalColors(colorRange)
            .dimension(resourcesDim)
            .group(resourcesTotal)
            .x(d3.scale.linear().domain([0, 5]))
            .elasticX(true)
            .label(function(d) {
                return d.key + " [" + d.value + "]";
            })
            .legend(dc.legend().x(40).y(10).itemHeight(10).gap(5));
    };

    resourcesTotal.all = function() {
        var newObject = [];
        for (var key in this) {
            if (this.hasOwnProperty(key) && key !== "all" && key !== "top") {
                newObject.push({
                    key: key,
                    value: this[key]
                });
            }
        }
        return newObject;
    };

    resourcesTotal.top = function(count) {
        var newObject = this.all();
        newObject.sort(function(a, b) {
            return b.value - a.value;
        });
        return newObject.slice(0, count);
    };
};
