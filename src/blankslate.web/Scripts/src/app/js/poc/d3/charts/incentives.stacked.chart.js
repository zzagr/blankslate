var IncentivesStackedBarChart = function(dataProvider) {

    this.Initialize = function() {
        var data = [{"key": "Stream0", "values": [{"x": 0, "y": 0.1978070508713224 }, {"x": 1, "y": 0.15210029112831236 }, {"x": 2, "y": 0.1828651961771794 }, {"x": 3, "y": 0.15910208151258998 }, {"x": 4, "y": 0.12947180365536606 }, {"x": 5, "y": 0.17523886926314586 }] }, {"key": "Stream1", "values": [{"x": 0, "y": 0.1936745670856908 }, {"x": 1, "y": 0.1580201807897538 }, {"x": 2, "y": 0.1267231597332284 }, {"x": 3, "y": 0.1265559283550829 }, {"x": 4, "y": 0.1916691974969581 }, {"x": 5, "y": 0.1939870560541749 }] }, {"key": "Stream2", "values": [{"x": 0, "y": 0.16678087759926533 }, {"x": 1, "y": 0.16617406729379164 }, {"x": 2, "y": 0.19544465986981624 }, {"x": 3, "y": 0.21138821516141026 }, {"x": 4, "y": 0.13129292425166136 }, {"x": 5, "y": 0.17233552957764775 }] }];
        
        var chartId = "#nv-stacked-incentives";
        var chart = nv.models.multiBarChart()
            .stacked(true)
            .showControls(false);

        d3.select(chartId)
            .selectAll('svg')
            .datum(data)
            .transition()
            .duration(0)
            .call(chart);
    };

};