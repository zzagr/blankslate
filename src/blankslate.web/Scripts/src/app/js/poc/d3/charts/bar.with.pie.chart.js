﻿function barChartWithPie(id, data, barTitle, pieTitle) {

    var divPieTable = "histogramContainer col-md-6 col-sm-6";
    var divBar = "histogramContainer col-md-12 col-sm-12";

    var tip = d3.tip()
      .attr('class', 'd3-tip')
      .offset([0, 0])
      .html(function (d) {
          return d.answerText + ": <span style='color:orangered'>" + d3.format("%")(d.totalChildResponses / data.totalChildResponses) + "</span>";
      });

    // calculate total frequency by segment for all state.
    var tF = data.childAnswerText.map(function (ca) {
        return {
            type: ca,
            freq: d3.sum(data.responses.map(
                function (rs) {
                    return d3.sum(rs.childResponses.map(
                        function (cr) {
                            return cr.answerText === ca ? cr.totalResponses : 0;
                        }));
                }))
        };
    });

    var colorDex = [];
    var index = 0;
    data.childAnswerText.map(function (ca) {
        colorDex[ca] = index++;
    });

    // calculate total frequency by state for all segment.
    var sF = data.responses.map(function (d) { return [d.answerText[0] + d.answerText[1], d.totalChildResponses]; });

    var colorRange = d3.scale.category10().range();

    var color = d3.scale.ordinal()
        .range(colorRange);

    var barColor = colorRange[0];

    // function to handle histogram.
    function histoGram(fD) {
        var hG = {}, hGDim = { t: 60, r: 0, b: 30, l: 0 };
        hGDim.w = 750 - hGDim.l - hGDim.r,
        hGDim.h = 300 - hGDim.t - hGDim.b;

        var container = d3.select(id).append("div").attr("class", divBar);
        container.append("h4").html(barTitle);
        //create svg for histogram.
        var hGsvg = container.append("svg")
            .attr("width", hGDim.w + hGDim.l + hGDim.r)
            .attr("height", hGDim.h + hGDim.t + hGDim.b).append("g")
            .attr("transform", "translate(" + hGDim.l + "," + hGDim.t + ")");

        

        hGsvg.call(tip);

        // create function for x-axis mapping.
        var x = d3.scale.ordinal().rangeRoundBands([0, hGDim.w], 0.1)
                .domain(fD.map(function (d) { return d[0]; }));

        // Add x-axis to the histogram svg.
        hGsvg.append("g").attr("class", "x axis")
            .attr("transform", "translate(0," + hGDim.h + ")")
            .call(d3.svg.axis().scale(x).orient("bottom"));

        // Create function for y-axis map.
        var y = d3.scale.linear().range([hGDim.h, 0])
                .domain([0, d3.max(fD, function (d) { return d[1]; })]);

        // Create bars for histogram to contain rectangles and freq labels.
        var bars = hGsvg.selectAll(".bar").data(fD).enter()
                .append("g").attr("class", "bar");

        //create the rectangles.
        bars.append("rect")
            .attr("x", function (d) { return x(d[0]); })
            .attr("y", function (d) { return y(d[1]); })
            .attr("width", x.rangeBand())
            .attr("height", function (d) { return hGDim.h - y(d[1]); })
            .attr('fill', barColor)
            .on("mouseover", mouseover)// mouseover is defined below.
            .on("mouseout", mouseout);// mouseout is defined below.

        //Create the frequency labels above the rectangles.
        bars.append("text").text(function (d) { return d3.format(",")(d[1]) })
            .attr("x", function (d) { return x(d[0]) + x.rangeBand() / 2; })
            .attr("y", function (d) { return y(d[1]) - 5; })
            .attr("text-anchor", "middle");

        function mouseover(d) {  // utility function to be called on mouseover.
            // filter for selected state.
            var st = data.responses.filter(function (s) { return s.answerText[0] + s.answerText[1] === d[0]; })[0];
            var nD = st.childResponses.map(function (s) { return { type: s.answerText, freq: s.totalResponses }; });

            // call update functions of pie-chart and legend.    
            pC.update(nD);
            leg.update(nD);
            tip.show(st);
        }

        function mouseout(d) {    // utility function to be called on mouseout.
            // reset the pie-chart and legend.    
            pC.update(tF);
            leg.update(tF);
            tip.hide();
        }

        // create function to update the bars. This will be used by pie-chart.
        hG.update = function (nD, color) {
            // update the domain of the y-axis map to reflect change in frequencies.
            y.domain([0, d3.max(nD, function (d) { return d[1]; })]);

            // Attach the new data to the bars.
            var bars = hGsvg.selectAll(".bar").data(nD);

            // transition the height and color of rectangles.
            bars.select("rect").transition().duration(500)
                .attr("y", function (d) { return y(d[1]); })
                .attr("height", function (d) { return hGDim.h - y(d[1]); })
                .attr("fill", color);

            // transition the frequency labels location and change value.
            bars.select("text").transition().duration(500)
                .text(function (d) { return d3.format(",")(d[1]) })
                .attr("y", function (d) { return y(d[1]) - 5; });
        }
        return hG;
    }


    // function to handle pieChart.
    function pieChart(pD) {
        var pC = {}, pieDim = { w: 300, h: 300 };
        pieDim.r = Math.min(pieDim.w, pieDim.h) / 2;

        // create container for the Pie and legend so that they wrap together on the same line.
        var container = d3.select(id).append("div").attr("class", divPieTable);
        container.append("h4").html(pieTitle);
        // create svg for pie chart.
        var piesvg = container.append("svg")
            .attr("width", pieDim.w).attr("height", pieDim.h).append("g")
            .attr("transform", "translate(" + pieDim.w / 2 + "," + pieDim.h / 2 + ")");

        // create function to draw the arcs of the pie slices.
        var arc = d3.svg.arc().outerRadius(pieDim.r - 10).innerRadius(0);

        // create a function to compute the pie slice angles.
        var pie = d3.layout.pie().sort(null).value(function (d) { return d.freq; });

        // Draw the pie slices.
        piesvg.selectAll("path").data(pie(pD)).enter().append("path").attr("d", arc)
            .each(function (d) { this._current = d; })
            .style("fill", function (d) { return color(colorDex[d.data.type]); })
            .on("mouseover", mouseover).on("mouseout", mouseout);

        // create function to update pie-chart. This will be used by histogram.
        pC.update = function (nD) {
            piesvg.selectAll("path").data(pie(nD)).transition().duration(500)
                .attrTween("d", arcTween);
        }
        // Utility function to be called on mouseover a pie slice.
        function mouseover(d) {
            // call the update function of histogram with new data.
            hG.update(data.responses.map(
                function (rs) {
                    return [
                        rs.answerText[0] + rs.answerText[1],
                        d3.sum(rs.childResponses.map(
                            function (cr) {
                                return cr.answerText === d.data.type ? cr.totalResponses : 0;
                            }
                        ))];
                }),
                color(colorDex[d.data.type]));
        }
        //Utility function to be called on mouseout a pie slice.
        function mouseout(d) {
            // call the update function of histogram with all data.
            hG.update(sF, barColor);
        }
        // Animating the pie-slice requiring a custom function which specifies
        // how the intermediate paths should be drawn.
        function arcTween(a) {
            var i = d3.interpolate(this._current, a);
            this._current = i(0);
            return function (t) { return arc(i(t)); };
        }
        return pC;
    }

    // function to handle legend.
    function legend(lD) {
        var leg = {};

        // create container for the Pie and legend so that they wrap together on the same line.
        var container = d3.select(id).append("div").attr("class", divPieTable);
        container.append("br");
        container.append("br");
        container.append("br");
        container.append("br");
        // create table for legend.
        var legend = container.append("table").attr('class', 'legend');

        // create one row per segment.
        var tr = legend.append("tbody").selectAll("tr").data(lD).enter().append("tr");

        // create the first column for each segment.
        tr.append("td").append("svg").attr("width", '16').attr("height", '16').append("rect")
            .attr("width", '16').attr("height", '16')
            .attr("fill", function (d) { return color(colorDex[d.type]); });

        // create the second column for each segment.
        tr.append("td").text(function (d) { return d.type; });

        // create the third column for each segment.
        tr.append("td").attr("class", 'legendFreq')
            .text(function (d) { return d3.format(",")(d.freq); });

        // create the fourth column for each segment.
        tr.append("td").attr("class", 'legendPerc')
            .text(function (d) { return getLegend(d, lD); });

        // Utility function to be used to update the legend.
        leg.update = function (nD) {
            // update the data attached to the row elements.
            var l = legend.select("tbody").selectAll("tr").data(nD);

            // update the frequencies.
            l.select(".legendFreq").text(function (d) { return d3.format(",")(d.freq); });

            // update the percentage column.
            l.select(".legendPerc").text(function (d) { return getLegend(d, nD); });
        }

        function getLegend(d, aD) { // Utility function to compute percentage.
            return d3.format("%")(d.freq / d3.sum(aD.map(function (v) { return v.freq; })));
        }

        return leg;
    }

    var hG = histoGram(sF); // create the histogram.
    var pC = pieChart(tF); // create the pie-chart.
    var leg = legend(tF);  // create the legend.


}