var IndustriesChart = function(dc, ndx, incentives) {
    var chartId = "#dc-bar-industries";
    var industriesChart = dc.rowChart(chartId);
    var industriesDim = ndx.dimension(dc.pluck('P01Q2'));
    var industriesTotal = industriesDim.group();
    var width = $(chartId).width();

    this.Initialize = function (colorRange) {
        industriesChart.width(width).height(300)
            .ordinalColors(colorRange)
            .dimension(industriesDim)
            .group(industriesTotal)
            .x(d3.scale.linear().domain([0, 5]))
            .elasticX(true)
            .label(function(d) {
                return d.key + " [" + d.value + "]";
            })
            .legend(dc.legend().x(40).y(10).itemHeight(10).gap(5))

        

        //since Bullet Charts are NVD3.js we listen for filter events on the
        //industries chart so we can redraw the incentives chart
        .on('filtered', function(chart) {
            //incentives.Redraw();
        });
    };
};
