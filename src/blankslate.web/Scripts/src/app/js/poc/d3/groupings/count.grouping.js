var CountGrouping = function() {
    this.reduceAdd = function(p, v) {
        ++p.count;
        return p;
    }

    this.reduceRemove = function(p, v) {
        --p.count;
        return p;
    }

    this.reduceInitial = function(p, v) {
        return {
            count: 0
        };
    }
};