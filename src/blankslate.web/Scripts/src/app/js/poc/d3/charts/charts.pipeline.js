
var ChartsPipeline = function(dc,ndx) {    
    //var incentivesBullet = new IncentivesBulletChart(new IncentivesBulletChartDataProvider(dc, ndx));
//    var incentivesStacked = new IncentivesStackedBarChart(new IncentivesStackedBarChartDataProvider(dc, ndx));
	var charts = [];
	charts.push(new IndustriesChart(dc,ndx, null));
	charts.push(new NumberOfEmployeesChart(dc,ndx));
	charts.push(new RegionsChart(dc,ndx));
	charts.push(new AttractingWorkersChart(dc,ndx));
	charts.push(new EmployeeRetentionChart(dc,ndx));
	charts.push(new GovernmentResourcesChart(dc,ndx));
	// charts.push(incentivesBullet);
    //	charts.push(incentivesStacked);

	var colorRange = d3.scale.category10().range();

	this.Initialize = function(){
		$.each(charts, function(index,value){
			value.Initialize(colorRange);
		});
	};
}