var EmployeeRetentionChart = function(dc, ndx) {
    var employeeRetentionChart = dc.pieChart("#dc-ring-employee-retention");
    var employeeRetentionDim = ndx.dimension(dc.pluck('P08Q1'));
    var employeeRetention = employeeRetentionDim.group();
    var all = ndx.groupAll();

    this.Initialize = function (colorRange) {
        employeeRetentionChart.width(200).height(200)
            .ordinalColors(colorRange)
            .dimension(employeeRetentionDim)
            .group(employeeRetention)
            .innerRadius(40)
            .label(function(d) {
                if (employeeRetentionChart.hasFilter() && !employeeRetentionChart.hasFilter(d.key)) {
                    return d.key + " (0%)";
                }
                var label = d.key;
                if (all.value()) {
                    label += " (" + Math.floor(d.value / all.value() * 100) + "%)";
                }
                return label;
            })
            .renderTitle(false);
    };
};
