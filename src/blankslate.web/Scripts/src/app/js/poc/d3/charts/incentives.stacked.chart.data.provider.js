var IncentivesStackedBarChartDataProvider = function (dc, ndx) {
    var grouping = new IncentivesGrouping();
    var incentivesDim = ndx.dimension(dc.pluck('P05Q1'));
    var incentivesTotal = incentivesDim.groupAll().reduce(
        grouping.reduceAdd,
        grouping.reduceRemove,
        grouping.reduceInitial).value();

    this.GetData = function() {
        var results = [];
        $.each(incentivesTotal.all(), function(k, v) {
            results.push(v.value);
        });
        return results;
    }

    incentivesTotal.all = function() {
        var newObject = [];

        for (var key in this) {
            if (this.hasOwnProperty(key) && key !== "all" && key !== "top") {
                var title = key;
                var min = this[key];
                newObject.push({
                    key: title,
                    value: {
                        series: 0,
                        size: min,
                        x: 0,
                        y: min,
                        y0: 0,
                        y1: min
                    }
                });
            }
        }
        return newObject;
    };

    incentivesTotal.top = function(count) {
        var newObject = this.all();
        newObject.sort(function(a, b) {
            return b.value - a.value;
        });
        return newObject.slice(0, count);
    };
};
