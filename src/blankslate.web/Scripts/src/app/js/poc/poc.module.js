(function() {
    'use strict';

    angular
        .module('app.poc', [
            'app.core'
        ]);
})();