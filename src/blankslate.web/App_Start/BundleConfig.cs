﻿using System.Web.Optimization;
using blankslate.util.Configuration;
using blankslate.util.Windsor;
using blankslate.web.util.Optimization;
using blankslate.web.util.Optimization.Orderers;

namespace blankslate.web
{
    public class BundleConfig
    {
        private const string AppDir = @"~/Scripts/src/app/";

        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/Content/lib").Include(
                "~/Scripts/lib/jquery/dist/jquery.js",
                "~/Scripts/lib/bootstrap/dist/js/bootstrap.js",
                "~/Scripts/lib/toastr/toastr.js",
                "~/Scripts/lib/angular/angular.js",
                "~/Scripts/lib/angular-ui-router/release/angular-ui-router.js",
                "~/Scripts/lib/angular-resource/angular-resource.js",
                "~/Scripts/lib/angular-animate/angular-animate.js",
                "~/Scripts/lib/angular-bootstrap/ui-bootstrap-tpls.js",
                //start:PoC with D3
                "~/Scripts/lib/d3/d3.js",
                "~/Scripts/lib/crossfilter/crossfilter.js",
                "~/Scripts/lib/dcjs/dc.js",
                //end:PoC with D3

                //start:PoC with AmCharts
                "~/Scripts/lib/amcharts/dist/amcharts/amcharts.js",
                "~/Scripts/lib/amcharts/dist/amcharts/pie.js",
                "~/Scripts/lib/amcharts/dist/amcharts/serial.js"
                //end:PoC with AmCharts

                ));


            bundles.AddBundle("~/Content/app", AppDir, "*.js", new AngularJsBundleOrderer());
            bundles.AddBundle("~/Content/charts", AppDir, "*.js", new D3BundleOrderer());

            bundles.Add(new StyleBundle("~/Content/css").Include(
                "~/Scripts/lib/font-awesome/css/font-awesome.css",
                "~/Scripts/lib/toastr/toastr.css",
                "~/Scripts/lib/bootstrap/dist/css/bootstrap.css",
                "~/Scripts/src/app/css/app.css"
                ));

            var settings = IoC.Resolve.Component<IWebSettings>();
            var details = settings.GetApplicationDetails();

            if (details.DeveloperMode) return;
            BundleTable.EnableOptimizations = true;
        }
    }
}