﻿using System.Net.Http.Formatting;
using System.Web.Http;
using System.Web.Http.Dispatcher;
using System.Web.Http.ExceptionHandling;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Castle.Windsor;
using blankslate.tasks.Windsor;
using blankslate.util.Windsor;
using blankslate.web;
using blankslate.web.Areas.Home.Controllers;
using blankslate.web.Areas.Ping.Controllers;
using blankslate.web.util.WebApi;
using blankslate.web.util.Windsor;

namespace blankslate.Web
{
    public class StartupTask
    {
        private static IWindsorContainer _container;

        public static void Start()
        {
            RegisterWindsor();
            RegisterWebApplication();
            SetJsonAsDefaultSerializer();
            RegisterUnhandledExceptionLogger();
        }

        private static void RegisterUnhandledExceptionLogger()
        {
            GlobalConfiguration.Configuration.Services.Add(typeof (IExceptionLogger), new UnhandledExceptionLogger());
        }

        private static void SetJsonAsDefaultSerializer()
        {
            var config = GlobalConfiguration.Configuration;
            config.Formatters.Clear();
            config.Formatters.Add(new JsonMediaTypeFormatter());
        }

        private static void RegisterWindsor()
        {
            _container = new WindsorContainer();
            _container.AddFacility<WebNhibernateFacility>();
            _container.Install(new ApiControllerInstaller<PingController>(),
                new MvcControllerInstaller<HomeController>(),
                new TaskContainerInstaller());

            //Register Service Locator
            IoC.Resolve = new ResolveWindsorContainer(_container);
            RegisterWindsorForControllers();
        }

        private static void RegisterWindsorForControllers()
        {
            GlobalConfiguration.Configuration.Services.Replace(typeof (IHttpControllerActivator), new ApiControllerActivator(_container));
            ControllerBuilder.Current.SetControllerFactory(new MvcControllerActivator(_container.Kernel));
        }

        private static void RegisterWebApplication()
        {
            AreaRegistration.RegisterAllAreas();

            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        public static void Shutdown()
        {
            _container.Dispose();
        }
    }
}